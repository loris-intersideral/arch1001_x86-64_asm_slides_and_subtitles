1
00:00:00,080 --> 00:00:04,160
so i said that typically i taught this

2
00:00:02,800 --> 00:00:05,520
and i would step through slides

3
00:00:04,160 --> 00:00:07,040
explaining every single one so that

4
00:00:05,520 --> 00:00:09,599
someone could read it later

5
00:00:07,040 --> 00:00:10,240
but thanks to the video format of this

6
00:00:09,599 --> 00:00:12,880
class

7
00:00:10,240 --> 00:00:14,639
now i can just save myself all that work

8
00:00:12,880 --> 00:00:18,000
of writing out all those slides

9
00:00:14,639 --> 00:00:21,039
and we'll do it live so

10
00:00:18,000 --> 00:00:23,600
here we are over in journey to the

11
00:00:21,039 --> 00:00:25,279
center of mem copy dot c

12
00:00:23,600 --> 00:00:27,519
so to get the show on the road we need

13
00:00:25,279 --> 00:00:28,080
to have a break point at the entry point

14
00:00:27,519 --> 00:00:30,240
of main

15
00:00:28,080 --> 00:00:34,160
and we want to step into mem copy so

16
00:00:30,240 --> 00:00:34,160
let's go ahead and start the debugger

17
00:00:35,280 --> 00:00:40,480
over to the disassembly view and let's

18
00:00:37,920 --> 00:00:44,000
step into step into step into

19
00:00:40,480 --> 00:00:46,320
until we step into the mem copy

20
00:00:44,000 --> 00:00:47,440
now you should expect just based on what

21
00:00:46,320 --> 00:00:49,600
the source code is

22
00:00:47,440 --> 00:00:51,280
that rcx which is the first argument is

23
00:00:49,600 --> 00:00:53,120
going to be the address of b

24
00:00:51,280 --> 00:00:55,520
and you can see in this mode where we

25
00:00:53,120 --> 00:00:57,360
have the symbol names on

26
00:00:55,520 --> 00:00:58,559
that that's the address of b and this is

27
00:00:57,360 --> 00:01:00,239
the address of a

28
00:00:58,559 --> 00:01:02,480
so that's fine you can go ahead and see

29
00:01:00,239 --> 00:01:04,719
the symbol names little extra help never

30
00:01:02,480 --> 00:01:04,719
hurt

31
00:01:05,040 --> 00:01:08,720
and r8 the third argument is going to be

32
00:01:07,600 --> 00:01:12,080
h that's the current

33
00:01:08,720 --> 00:01:15,840
size of our struct so let's step

34
00:01:12,080 --> 00:01:18,320
into and there's some jump to mem copy

35
00:01:15,840 --> 00:01:20,400
sure whatever all right and now we are

36
00:01:18,320 --> 00:01:22,240
at the mem copy assembly so

37
00:01:20,400 --> 00:01:23,439
i'm going to start writing down a little

38
00:01:22,240 --> 00:01:25,040
bit of notes and

39
00:01:23,439 --> 00:01:27,840
pseudo code in order to help me remember

40
00:01:25,040 --> 00:01:30,960
what's going on here

41
00:01:27,840 --> 00:01:35,119
so first of all rcx into r11 so

42
00:01:30,960 --> 00:01:38,159
r11 equals rcx and what is rcx

43
00:01:35,119 --> 00:01:40,880
it's the first argument to mem copy

44
00:01:38,159 --> 00:01:43,840
so that is going to be the address of b

45
00:01:40,880 --> 00:01:43,840
our destination

46
00:01:44,320 --> 00:01:51,040
then we have rdx into r10 so r10

47
00:01:47,920 --> 00:01:55,119
equals rdx equals the address of

48
00:01:51,040 --> 00:01:58,640
a our source

49
00:01:55,119 --> 00:02:03,119
and then you see a comparison

50
00:01:58,640 --> 00:02:04,159
of r8 to r10 well maybe a little preface

51
00:02:03,119 --> 00:02:07,360
here that it was

52
00:02:04,159 --> 00:02:11,120
r8 which got the length

53
00:02:07,360 --> 00:02:14,239
in the mem copy call so an immediate

54
00:02:11,120 --> 00:02:17,760
check of is r8 below

55
00:02:14,239 --> 00:02:19,760
or equal 10 hex 10. so what would that

56
00:02:17,760 --> 00:02:22,879
look like in pseudocode

57
00:02:19,760 --> 00:02:26,319
that would be something like if

58
00:02:22,879 --> 00:02:29,200
r8 which is lang is

59
00:02:26,319 --> 00:02:30,560
less than or equal to below or equal so

60
00:02:29,200 --> 00:02:33,920
what is below

61
00:02:30,560 --> 00:02:37,200
below is the unsigned notion

62
00:02:33,920 --> 00:02:40,720
so is it below hex 10 and

63
00:02:37,200 --> 00:02:43,920
if so go to

64
00:02:40,720 --> 00:02:43,920
this particular address

65
00:02:46,080 --> 00:02:50,080
all right so let's see if that's going

66
00:02:47,440 --> 00:02:50,080
to be taken

67
00:02:51,680 --> 00:02:56,800
and we know from r8 is

68
00:02:54,800 --> 00:02:58,720
eight because our size of our struct is

69
00:02:56,800 --> 00:03:00,720
eight so eight is less than ten

70
00:02:58,720 --> 00:03:02,080
so we know that that's going to be taken

71
00:03:00,720 --> 00:03:03,440
step into

72
00:03:02,080 --> 00:03:06,400
and there we go we're now at that

73
00:03:03,440 --> 00:03:09,680
address so let's go ahead and write out

74
00:03:06,400 --> 00:03:09,680
what is at that address

75
00:03:11,040 --> 00:03:15,920
so we have move rcx into our

76
00:03:16,840 --> 00:03:19,840
ax

77
00:03:20,080 --> 00:03:25,040
and let's just go ahead and call it like

78
00:03:23,040 --> 00:03:28,400
it is

79
00:03:25,040 --> 00:03:31,680
the address would be our destination

80
00:03:28,400 --> 00:03:32,319
next we have lea of some random memory

81
00:03:31,680 --> 00:03:34,400
address

82
00:03:32,319 --> 00:03:35,519
into r9 so that's literally just going

83
00:03:34,400 --> 00:03:39,040
to take this exact

84
00:03:35,519 --> 00:03:39,040
value and put it into r9

85
00:03:39,120 --> 00:03:44,879
and we can confirm that by stepping over

86
00:03:42,000 --> 00:03:44,879
it in the assembly

87
00:03:45,599 --> 00:03:49,280
step step and r nine is indeed a seven f

88
00:03:48,959 --> 00:03:51,760
f

89
00:03:49,280 --> 00:03:52,400
b seven sixty four nine zero zero zero

90
00:03:51,760 --> 00:03:56,159
seven f

91
00:03:52,400 --> 00:03:58,159
f b seven sixty nine zero zero zero

92
00:03:56,159 --> 00:03:59,599
all right next we have r nine which is

93
00:03:58,159 --> 00:04:02,879
that number

94
00:03:59,599 --> 00:04:06,319
plus our eight which is our size

95
00:04:02,879 --> 00:04:08,080
times four plus twenty seven thousand

96
00:04:06,319 --> 00:04:09,840
don't know what's up with that but

97
00:04:08,080 --> 00:04:12,840
whatever let's just see what gets

98
00:04:09,840 --> 00:04:15,280
plucked out of memory there and put into

99
00:04:12,840 --> 00:04:20,079
ecx

100
00:04:15,280 --> 00:04:21,759
step ecx rcx gets this value

101
00:04:20,079 --> 00:04:24,079
so let's just go ahead and note that

102
00:04:21,759 --> 00:04:24,079
down

103
00:04:25,600 --> 00:04:32,240
and then it adds r9

104
00:04:28,800 --> 00:04:36,000
r9 has this value

105
00:04:32,240 --> 00:04:40,960
which we wrote right there so basically

106
00:04:36,000 --> 00:04:43,840
rcx is getting a plus equals

107
00:04:40,960 --> 00:04:43,840
of that value

108
00:04:51,680 --> 00:04:55,040
and then what does it do with it it

109
00:04:53,600 --> 00:04:58,080
actually uses it as

110
00:04:55,040 --> 00:05:01,039
a address to jump to so this is the

111
00:04:58,080 --> 00:05:01,840
jump indirect so it's just calculating

112
00:05:01,039 --> 00:05:04,080
an address

113
00:05:01,840 --> 00:05:06,320
and then it's jumping to that location

114
00:05:04,080 --> 00:05:07,600
so that is a new address

115
00:05:06,320 --> 00:05:10,479
we're gonna find out what's at that

116
00:05:07,600 --> 00:05:15,039
address so basically it's just doing

117
00:05:10,479 --> 00:05:18,080
go to rcx which is that

118
00:05:15,039 --> 00:05:20,639
let's put that down and say what is the

119
00:05:18,080 --> 00:05:24,160
code there

120
00:05:20,639 --> 00:05:27,600
step into and what do we have we have a

121
00:05:24,160 --> 00:05:30,160
move quad word from rdx well what is rdx

122
00:05:27,600 --> 00:05:30,160
at this point

123
00:05:30,800 --> 00:05:34,639
rdx is still the address of a so it's

124
00:05:33,759 --> 00:05:37,280
plucking

125
00:05:34,639 --> 00:05:38,479
eight bytes keyword pointer clicking

126
00:05:37,280 --> 00:05:41,039
eight bytes

127
00:05:38,479 --> 00:05:42,160
out of the source and putting it into

128
00:05:41,039 --> 00:05:44,240
rcx

129
00:05:42,160 --> 00:05:46,240
what does it do next it takes that eight

130
00:05:44,240 --> 00:05:51,520
bytes and it puts it into

131
00:05:46,240 --> 00:05:54,560
the memory specified by rax what is rax

132
00:05:51,520 --> 00:05:56,720
rax is rcx is the address of b so it's

133
00:05:54,560 --> 00:05:58,240
plucking eight bytes out of the source

134
00:05:56,720 --> 00:06:00,000
and writing eight bytes to the

135
00:05:58,240 --> 00:06:02,400
destination using two

136
00:06:00,000 --> 00:06:04,880
move instructions and it can do that

137
00:06:02,400 --> 00:06:08,319
because r8 which is our length

138
00:06:04,880 --> 00:06:11,919
was 8 bytes so that

139
00:06:08,319 --> 00:06:13,840
is doing the mem copy and we're done

140
00:06:11,919 --> 00:06:16,160
we're done with the mem copy yay

141
00:06:13,840 --> 00:06:18,080
except we didn't find the elusive rep

142
00:06:16,160 --> 00:06:21,360
move s which we're searching for

143
00:06:18,080 --> 00:06:23,199
so that's no good we need to try try

144
00:06:21,360 --> 00:06:25,280
again

145
00:06:23,199 --> 00:06:26,319
so based on this pseudo code we have

146
00:06:25,280 --> 00:06:28,240
right now

147
00:06:26,319 --> 00:06:29,919
the most interesting bit the only sort

148
00:06:28,240 --> 00:06:30,960
of conditional control flow bit that we

149
00:06:29,919 --> 00:06:33,199
ran into

150
00:06:30,960 --> 00:06:34,639
is this check of if the length is less

151
00:06:33,199 --> 00:06:37,759
than or equal to 10 it go

152
00:06:34,639 --> 00:06:40,160
here so we want to not go there

153
00:06:37,759 --> 00:06:41,520
so we need to make sure that the length

154
00:06:40,160 --> 00:06:44,960
is greater than

155
00:06:41,520 --> 00:06:48,560
x10 so we can easily do that by

156
00:06:44,960 --> 00:06:51,599
sticking in x10 in this variable

157
00:06:48,560 --> 00:06:51,919
2 and now hex 10 plus 4 for the int will

158
00:06:51,599 --> 00:06:54,720
be

159
00:06:51,919 --> 00:06:57,680
definitely greater than hex 10. so let's

160
00:06:54,720 --> 00:06:57,680
start it all over again

161
00:06:58,639 --> 00:07:06,479
boo diagnostics

162
00:07:02,880 --> 00:07:07,840
all right disassembly step step step

163
00:07:06,479 --> 00:07:12,080
step step step

164
00:07:07,840 --> 00:07:15,199
into the breach step step step

165
00:07:12,080 --> 00:07:18,240
all right now this time r8 is

166
00:07:15,199 --> 00:07:20,800
x20 sorry decimal 20

167
00:07:18,240 --> 00:07:22,400
which is greater than x10 so it's not

168
00:07:20,800 --> 00:07:24,960
going to take the jump

169
00:07:22,400 --> 00:07:25,919
boom but then we have an immediate

170
00:07:24,960 --> 00:07:29,280
compare of

171
00:07:25,919 --> 00:07:32,960
r8 against hex20 so 22

172
00:07:29,280 --> 00:07:36,880
hex sorry x sorry decimal 20

173
00:07:32,960 --> 00:07:38,960
to decimal 32 and jump if below or equal

174
00:07:36,880 --> 00:07:41,199
well that's definitely blue or equal

175
00:07:38,960 --> 00:07:42,560
so let's update our pseudo code if you

176
00:07:41,199 --> 00:07:46,160
don't take the jump

177
00:07:42,560 --> 00:07:46,639
then r8 which is your length and now our

178
00:07:46,160 --> 00:07:50,160
length

179
00:07:46,639 --> 00:07:52,720
is x 14.

180
00:07:50,160 --> 00:07:55,039
if the length is less than or equal to

181
00:07:52,720 --> 00:07:57,120
hex 20

182
00:07:55,039 --> 00:08:00,240
then where's it going to go it's going

183
00:07:57,120 --> 00:08:00,240
to go right there

184
00:08:07,840 --> 00:08:11,360
so because that's the value right now it

185
00:08:10,080 --> 00:08:13,440
will take that jump

186
00:08:11,360 --> 00:08:14,720
and where does it land a bunch of move

187
00:08:13,440 --> 00:08:18,160
ups with

188
00:08:14,720 --> 00:08:19,919
xmmo registers and xmm word pointers

189
00:08:18,160 --> 00:08:22,319
and that's scary so we're just going to

190
00:08:19,919 --> 00:08:22,319
escape

191
00:08:22,400 --> 00:08:27,039
okay there we go i just stepped and i

192
00:08:24,639 --> 00:08:28,960
eventually stepped out of the mem copy

193
00:08:27,039 --> 00:08:30,319
well the main point there is that we did

194
00:08:28,960 --> 00:08:32,959
not see rep move s

195
00:08:30,319 --> 00:08:35,279
on that path and so we gotta try try

196
00:08:32,959 --> 00:08:35,279
again

197
00:08:35,599 --> 00:08:40,240
so i'll update my pseudo code a little

198
00:08:37,440 --> 00:08:43,680
bit before i do that just to say that

199
00:08:40,240 --> 00:08:43,680
at this particular address

200
00:08:45,519 --> 00:08:53,040
that was this first path was

201
00:08:49,600 --> 00:08:56,240
to move instructions

202
00:08:53,040 --> 00:08:59,600
copying eight bytes

203
00:08:56,240 --> 00:09:03,480
and then returning and this path is some

204
00:08:59,600 --> 00:09:08,160
move s move ups

205
00:09:03,480 --> 00:09:08,160
instructions which are scary

206
00:09:10,080 --> 00:09:14,880
so once again we have a situation where

207
00:09:13,040 --> 00:09:17,600
the only conditional control flow

208
00:09:14,880 --> 00:09:18,880
was the hex10 check which we bypassed by

209
00:09:17,600 --> 00:09:21,920
increasing the size

210
00:09:18,880 --> 00:09:23,839
now there's a hex 20 check and we should

211
00:09:21,920 --> 00:09:25,200
probably bypass that by increasing the

212
00:09:23,839 --> 00:09:29,680
size

213
00:09:25,200 --> 00:09:34,839
so hex 20 let's go

214
00:09:29,680 --> 00:09:37,839
disassembly step step step step step

215
00:09:34,839 --> 00:09:37,839
step

216
00:09:38,160 --> 00:09:40,880
now our

217
00:09:42,000 --> 00:09:45,920
r8 is hex 24

218
00:09:46,240 --> 00:09:50,720
so we're going to step step step step

219
00:09:48,800 --> 00:09:53,920
until we get to this compare against

220
00:09:50,720 --> 00:09:55,920
x20 we are greater than hex 20 now

221
00:09:53,920 --> 00:09:57,839
hold on a sec let's get this into hex

222
00:09:55,920 --> 00:10:01,760
view

223
00:09:57,839 --> 00:10:01,760
hexadecimal display much better

224
00:10:02,959 --> 00:10:07,120
all right so now it is not going to take

225
00:10:05,279 --> 00:10:08,399
this jump below or equal so it's going

226
00:10:07,120 --> 00:10:10,480
to fall through

227
00:10:08,399 --> 00:10:12,240
so let's go ahead and fall through and

228
00:10:10,480 --> 00:10:15,839
let's get a sense of

229
00:10:12,240 --> 00:10:16,560
what's coming up next so didn't take the

230
00:10:15,839 --> 00:10:19,040
jump below

231
00:10:16,560 --> 00:10:22,720
now we fall through let's pseudo code

232
00:10:19,040 --> 00:10:22,720
out what happens if we fall through

233
00:10:24,959 --> 00:10:31,360
so it's going to take

234
00:10:28,160 --> 00:10:34,079
rdx and subtract rcx from it and stick

235
00:10:31,360 --> 00:10:35,200
the value back into rdx well what is rdx

236
00:10:34,079 --> 00:10:38,720
at this point

237
00:10:35,200 --> 00:10:41,760
rdx is the source

238
00:10:38,720 --> 00:10:44,839
so it's basically rd

239
00:10:41,760 --> 00:10:49,760
rdx equals rdx minus

240
00:10:44,839 --> 00:10:53,360
rcx which basically means

241
00:10:49,760 --> 00:10:56,399
which basically means take the

242
00:10:53,360 --> 00:11:01,839
source address and subtract the

243
00:10:56,399 --> 00:11:01,839
destination address

244
00:11:01,920 --> 00:11:05,600
okay it does that subtract but then

245
00:11:03,680 --> 00:11:08,320
there's an immediate jump

246
00:11:05,600 --> 00:11:09,920
above or equal so it's kind of like

247
00:11:08,320 --> 00:11:11,920
that's being used as a compare

248
00:11:09,920 --> 00:11:13,680
except that they are actually still

249
00:11:11,920 --> 00:11:16,720
storing the result of that comparison

250
00:11:13,680 --> 00:11:16,720
back into rdx

251
00:11:17,200 --> 00:11:20,800
so jump above or equal is this going to

252
00:11:19,839 --> 00:11:23,279
be taken

253
00:11:20,800 --> 00:11:24,000
well the way we would interpret that is

254
00:11:23,279 --> 00:11:27,120
is this

255
00:11:24,000 --> 00:11:28,880
greater than or equal to rcx so let's

256
00:11:27,120 --> 00:11:32,480
look at the registers quick

257
00:11:28,880 --> 00:11:36,959
rdx 14 fdb0

258
00:11:32,480 --> 00:11:40,240
and rcx14fd8

259
00:11:36,959 --> 00:11:40,880
so is this greater than this the answer

260
00:11:40,240 --> 00:11:44,480
is

261
00:11:40,880 --> 00:11:47,920
no this one ends in b0

262
00:11:44,480 --> 00:11:48,800
this one ends in d0 so cx is greater

263
00:11:47,920 --> 00:11:51,040
than dx

264
00:11:48,800 --> 00:11:52,320
so we don't expect this jump to actually

265
00:11:51,040 --> 00:11:55,760
be taken

266
00:11:52,320 --> 00:11:57,519
so do so we're going to put in our

267
00:11:55,760 --> 00:11:59,600
conditional control flow we're going to

268
00:11:57,519 --> 00:12:03,200
say well they're going to use this for

269
00:11:59,600 --> 00:12:06,320
if rdx

270
00:12:03,200 --> 00:12:11,200
let's just put yeah if rdx

271
00:12:06,320 --> 00:12:15,519
which is the source is above or equal

272
00:12:11,200 --> 00:12:15,519
rcx which is the dust

273
00:12:16,000 --> 00:12:22,480
then it's going to go to

274
00:12:20,000 --> 00:12:24,480
here but it is not going to actually

275
00:12:22,480 --> 00:12:29,600
take that

276
00:12:24,480 --> 00:12:32,560
jump this time because it is not

277
00:12:29,600 --> 00:12:33,040
so let's keep stepping didn't take the

278
00:12:32,560 --> 00:12:35,920
jump

279
00:12:33,040 --> 00:12:39,839
now we have an lea so what's going on

280
00:12:35,920 --> 00:12:39,839
with the lea

281
00:12:43,200 --> 00:12:49,440
it is doing r8 plus r10 and it's putting

282
00:12:46,720 --> 00:12:50,320
it into rax well what is r8 r8 is our

283
00:12:49,440 --> 00:12:53,360
length

284
00:12:50,320 --> 00:12:55,600
our 10 is our source

285
00:12:53,360 --> 00:12:58,160
so it's basically going to say rax

286
00:12:55,600 --> 00:12:58,160
equals

287
00:12:58,959 --> 00:13:02,480
r10 plus

288
00:13:02,560 --> 00:13:09,760
source plus r8 which is

289
00:13:06,079 --> 00:13:12,240
length but it's basically the address

290
00:13:09,760 --> 00:13:12,240
of that

291
00:13:13,440 --> 00:13:17,680
right this is basically an address this

292
00:13:15,839 --> 00:13:19,519
is basically a size and so it's going to

293
00:13:17,680 --> 00:13:22,800
get a new address which is basically the

294
00:13:19,519 --> 00:13:25,519
end of the source buffer so end of the

295
00:13:22,800 --> 00:13:25,519
source buffer

296
00:13:32,399 --> 00:13:36,320
all right and what's next it's going to

297
00:13:34,240 --> 00:13:40,800
compare rcx

298
00:13:36,320 --> 00:13:43,839
to rax so compare

299
00:13:40,800 --> 00:13:47,279
is if r c

300
00:13:43,839 --> 00:13:50,399
x is below so

301
00:13:47,279 --> 00:13:53,760
if rcx is strictly below

302
00:13:50,399 --> 00:14:03,839
rax then

303
00:13:53,760 --> 00:14:03,839
it's going to take this jump

304
00:14:04,399 --> 00:14:08,639
so let's do a little bit of

305
00:14:06,279 --> 00:14:09,920
interpretation of this stuff that we're

306
00:14:08,639 --> 00:14:15,360
seeing so far

307
00:14:09,920 --> 00:14:17,760
let's add some curly braces

308
00:14:15,360 --> 00:14:19,360
so let's look at this sort of nested if

309
00:14:17,760 --> 00:14:22,160
condition that we have

310
00:14:19,360 --> 00:14:23,279
so we have if source is greater than or

311
00:14:22,160 --> 00:14:28,079
equal to dest

312
00:14:23,279 --> 00:14:28,079
that implies that the else is if

313
00:14:29,519 --> 00:14:36,800
if source is less than

314
00:14:33,120 --> 00:14:39,839
dust and then what is this

315
00:14:36,800 --> 00:14:45,680
rax was this end of source buffer

316
00:14:39,839 --> 00:14:48,880
and rcx is equal to the dust

317
00:14:45,680 --> 00:14:52,079
so this is basically if

318
00:14:48,880 --> 00:14:59,839
dust is less than

319
00:14:52,079 --> 00:14:59,839
source plus length

320
00:15:00,000 --> 00:15:03,279
and because this if is nested inside of

321
00:15:02,880 --> 00:15:05,519
this

322
00:15:03,279 --> 00:15:06,560
if the way that you actually get to this

323
00:15:05,519 --> 00:15:12,880
go to

324
00:15:06,560 --> 00:15:18,959
is if source is less than dust

325
00:15:12,880 --> 00:15:18,959
and dust is less than source plus length

326
00:15:20,160 --> 00:15:29,600
ah what is it doing there we go

327
00:15:26,000 --> 00:15:32,160
so you go here if essentially

328
00:15:29,600 --> 00:15:33,040
your destination is overlapping with

329
00:15:32,160 --> 00:15:35,199
your source

330
00:15:33,040 --> 00:15:37,360
so destination is greater than source

331
00:15:35,199 --> 00:15:38,959
but it's less than source plus length

332
00:15:37,360 --> 00:15:40,399
and that means the place that you're mem

333
00:15:38,959 --> 00:15:42,639
copying into

334
00:15:40,399 --> 00:15:44,320
is going to be a place that is within

335
00:15:42,639 --> 00:15:46,639
the source buffer

336
00:15:44,320 --> 00:15:48,480
so that could potentially be a problem

337
00:15:46,639 --> 00:15:50,720
but let's see whether it actually takes

338
00:15:48,480 --> 00:15:54,639
that jump here

339
00:15:50,720 --> 00:15:57,279
so compare jump below

340
00:15:54,639 --> 00:15:59,199
and it did not take that jump so it just

341
00:15:57,279 --> 00:16:02,560
fell through to this next thing

342
00:15:59,199 --> 00:16:05,600
compare r8 to hex 80. so

343
00:16:02,560 --> 00:16:07,040
great we didn't do any of that so let's

344
00:16:05,600 --> 00:16:11,279
just go out here

345
00:16:07,040 --> 00:16:13,680
and say if r8

346
00:16:11,279 --> 00:16:14,880
less than hex 80. now why am i putting

347
00:16:13,680 --> 00:16:17,360
it out here

348
00:16:14,880 --> 00:16:20,240
well because it turns out that if you

349
00:16:17,360 --> 00:16:23,279
look back at the actual addresses here

350
00:16:20,240 --> 00:16:24,639
this jump above right here actually

351
00:16:23,279 --> 00:16:28,240
transfers down to here

352
00:16:24,639 --> 00:16:31,360
so this is a little nested condition

353
00:16:28,240 --> 00:16:32,880
inside of here and if this condition

354
00:16:31,360 --> 00:16:35,680
doesn't hold then you just kind of jump

355
00:16:32,880 --> 00:16:39,120
over this extra check

356
00:16:35,680 --> 00:16:40,720
so you know probably the better place to

357
00:16:39,120 --> 00:16:43,440
put it would be

358
00:16:40,720 --> 00:16:43,440
right here

359
00:16:47,040 --> 00:16:50,320
so i guess i'll just put it there for

360
00:16:48,839 --> 00:16:52,639
now

361
00:16:50,320 --> 00:16:54,800
all right compare against hex 80 well we

362
00:16:52,639 --> 00:16:58,959
know that we're not greater than hex 80.

363
00:16:54,800 --> 00:16:58,959
so is it going to take this below you

364
00:17:02,839 --> 00:17:08,959
betcha

365
00:17:05,760 --> 00:17:12,000
all right take the jump and

366
00:17:08,959 --> 00:17:13,360
ah some more move ups let's get out of

367
00:17:12,000 --> 00:17:16,319
here

368
00:17:13,360 --> 00:17:19,839
step step let's step until the return

369
00:17:16,319 --> 00:17:19,839
head for the exit

370
00:17:24,720 --> 00:17:31,360
there we go we're out we're safe

371
00:17:28,240 --> 00:17:33,039
well this again suggests that we need to

372
00:17:31,360 --> 00:17:34,960
do something that's length greater than

373
00:17:33,039 --> 00:17:37,120
hex 80.

374
00:17:34,960 --> 00:17:38,640
so let's go ahead and put that down here

375
00:17:37,120 --> 00:17:44,160
somewhere

376
00:17:38,640 --> 00:17:44,160
49 15 30 49 13 30 yep

377
00:17:45,440 --> 00:17:52,640
so let's loop down

378
00:17:49,200 --> 00:17:56,480
that's on that path let's

379
00:17:52,640 --> 00:17:56,480
bump the size to hex 80

380
00:17:56,880 --> 00:18:01,840
and once more

381
00:18:08,320 --> 00:18:12,080
and now we know that we are greater than

382
00:18:10,240 --> 00:18:15,520
x80 so this below or equal

383
00:18:12,080 --> 00:18:16,320
is not going to be taken so it'll just

384
00:18:15,520 --> 00:18:19,360
fall through

385
00:18:16,320 --> 00:18:23,760
to the next stuff so test

386
00:18:19,360 --> 00:18:29,840
all right so if this

387
00:18:23,760 --> 00:18:29,840
go there

388
00:18:35,120 --> 00:18:42,240
stuff so it's doing some sort of

389
00:18:38,799 --> 00:18:45,039
test of a byte pointer against

390
00:18:42,240 --> 00:18:45,600
two well let's check what's at that byte

391
00:18:45,039 --> 00:18:47,840
pointer

392
00:18:45,600 --> 00:18:50,400
paste that in and it looks like the

393
00:18:47,840 --> 00:18:52,160
memory at that address is actually two

394
00:18:50,400 --> 00:18:54,400
and it's a byte pointer so we may as

395
00:18:52,160 --> 00:18:55,760
well display it one byte at a time

396
00:18:54,400 --> 00:18:58,240
so it's going to pluck memory out of

397
00:18:55,760 --> 00:18:59,200
that value and it's going to test it

398
00:18:58,240 --> 00:19:02,000
against two now

399
00:18:59,200 --> 00:19:03,919
test we said is an and instruction that

400
00:19:02,000 --> 00:19:07,520
throws away the results

401
00:19:03,919 --> 00:19:09,280
so the way we might interpret this

402
00:19:07,520 --> 00:19:10,799
and there's a jump equal immediately

403
00:19:09,280 --> 00:19:15,600
after so

404
00:19:10,799 --> 00:19:15,600
it's an end with a jump equal so if

405
00:19:17,520 --> 00:19:20,320
some value

406
00:19:22,720 --> 00:19:25,840
equals zero

407
00:19:26,000 --> 00:19:30,960
then it's going to take the jump

408
00:19:34,960 --> 00:19:38,480
and what is that value well that value

409
00:19:37,520 --> 00:19:42,480
is this

410
00:19:38,480 --> 00:19:45,200
byte pointer anded with two

411
00:19:42,480 --> 00:19:46,799
so i might say that this is the

412
00:19:45,200 --> 00:19:50,320
dereferencing

413
00:19:46,799 --> 00:19:54,640
of a byte pointer char star

414
00:19:50,320 --> 00:19:57,679
not a car char char star

415
00:19:54,640 --> 00:19:57,679
of this value

416
00:19:57,760 --> 00:20:03,679
dereferenced so get to memory

417
00:20:01,280 --> 00:20:04,640
grab that value out it's grabbing a byte

418
00:20:03,679 --> 00:20:08,240
because it's a char

419
00:20:04,640 --> 00:20:12,240
star and then bit was and it

420
00:20:08,240 --> 00:20:12,960
with two so if the result of that bit

421
00:20:12,240 --> 00:20:15,440
wise and

422
00:20:12,960 --> 00:20:17,520
is equal to zero then it's going to take

423
00:20:15,440 --> 00:20:19,840
the jump and if not it's going to

424
00:20:17,520 --> 00:20:21,120
go somewhere else you can see there's a

425
00:20:19,840 --> 00:20:24,400
hard coded jump

426
00:20:21,120 --> 00:20:24,400
if it doesn't take that

427
00:20:25,360 --> 00:20:28,080
so if

428
00:20:28,480 --> 00:20:30,960
else

429
00:20:32,320 --> 00:20:35,440
and we looked at memory so we know that

430
00:20:34,880 --> 00:20:39,039
it

431
00:20:35,440 --> 00:20:41,200
the value there is two so the result

432
00:20:39,039 --> 00:20:42,240
is not going to be zero the result of

433
00:20:41,200 --> 00:20:44,240
this

434
00:20:42,240 --> 00:20:45,600
operation is not going to be zero so

435
00:20:44,240 --> 00:20:49,840
it's not going to take that

436
00:20:45,600 --> 00:20:49,840
it's going to take this

437
00:20:58,480 --> 00:21:04,080
my wife doesn't like it when i don't put

438
00:21:00,640 --> 00:21:04,080
spaces after my comments

439
00:21:06,720 --> 00:21:10,159
all right skipped over the jump equal

440
00:21:09,200 --> 00:21:13,919
take the

441
00:21:10,159 --> 00:21:13,919
jump and where do we land

442
00:21:14,080 --> 00:21:20,559
rep move us here it is folks this is

443
00:21:17,039 --> 00:21:20,559
what we've been searching for

444
00:21:24,720 --> 00:21:33,200
so i'm gonna go ahead and copy that out

445
00:21:30,240 --> 00:21:36,840
and we will take a look at what's up

446
00:21:33,200 --> 00:21:39,840
with that after we learn more about this

447
00:21:36,840 --> 00:21:39,840
instruction

