1
00:00:00,080 --> 00:00:04,799
so the takeaways from IfExample.c

2
00:00:02,879 --> 00:00:05,600
is that conditional logic like if

3
00:00:04,799 --> 00:00:08,960
statements

4
00:00:05,600 --> 00:00:11,840
turns into jcc jump condition code

5
00:00:08,960 --> 00:00:12,799
type jumps, where it says if this

6
00:00:11,840 --> 00:00:14,799
condition is true

7
00:00:12,799 --> 00:00:16,160
I shall jump to the address specified, if

8
00:00:14,799 --> 00:00:17,520
not I will fall through

9
00:00:16,160 --> 00:00:19,520
and just go to the next assembly

10
00:00:17,520 --> 00:00:20,800
instruction. But here you can see that

11
00:00:19,520 --> 00:00:22,000
it'll eventually run into this

12
00:00:20,800 --> 00:00:23,760
unconditional jump

13
00:00:22,000 --> 00:00:25,039
which will bounce it down towards the

14
00:00:23,760 --> 00:00:27,519
end of the

15
00:00:25,039 --> 00:00:29,039
function. Very frequently when you see

16
00:00:27,519 --> 00:00:31,679
checks that are involving

17
00:00:29,039 --> 00:00:32,079
equality or inequality such as we have

18
00:00:31,679 --> 00:00:34,239
here

19
00:00:32,079 --> 00:00:35,200
that's going to turn into a cmp

20
00:00:34,239 --> 00:00:37,680
instruction.

21
00:00:35,200 --> 00:00:39,200
And cmp again is just a sub

22
00:00:37,680 --> 00:00:39,760
instruction where you throw away the

23
00:00:39,200 --> 00:00:41,360
result.

24
00:00:39,760 --> 00:00:42,879
So it's going to take this, it's going to

25
00:00:41,360 --> 00:00:44,480
subtract that,

26
00:00:42,879 --> 00:00:46,239
but then it's not going to store it back

27
00:00:44,480 --> 00:00:48,239
into memory. It's just going to set the

28
00:00:46,239 --> 00:00:50,160
flags and throw the result away.

29
00:00:48,239 --> 00:00:52,160
And after it set those flags it's

30
00:00:50,160 --> 00:00:54,000
ultimately the RFLAGS register,

31
00:00:52,160 --> 00:00:55,360
which these conditional jump

32
00:00:54,000 --> 00:00:56,800
instructions are checking

33
00:00:55,360 --> 00:00:58,960
for whether or not they're going to take

34
00:00:56,800 --> 00:01:00,559
the jump. Okay, so we just picked up

35
00:00:58,960 --> 00:01:03,120
two quote unquote new assembly

36
00:01:00,559 --> 00:01:06,560
instructions: the family of Jcc

37
00:01:03,120 --> 00:01:08,080
and cmp. So where does Jcc show up on

38
00:01:06,560 --> 00:01:11,760
the pie chart?

39
00:01:08,080 --> 00:01:14,799
Multiple places: we've got at 1% js

40
00:01:11,760 --> 00:01:16,560
jump sign. So I said sign bit is one of

41
00:01:14,799 --> 00:01:18,960
the bits I want you to care about, that's

42
00:01:16,560 --> 00:01:19,360
if the most significant bit of result is

43
00:01:18,960 --> 00:01:22,080
one

44
00:01:19,360 --> 00:01:24,720
then the signed flag will be set. And

45
00:01:22,080 --> 00:01:25,439
jnz and jz so the zero

46
00:01:24,720 --> 00:01:28,320
flag

47
00:01:25,439 --> 00:01:29,439
jumps if it's zero flag is not set to

48
00:01:28,320 --> 00:01:32,240
1 and jumps if

49
00:01:29,439 --> 00:01:32,799
zero flag is set to 1. So you can see

50
00:01:32,240 --> 00:01:35,600
even just

51
00:01:32,799 --> 00:01:37,360
statistically like my intuition from my

52
00:01:35,600 --> 00:01:39,759
personal experience of

53
00:01:37,360 --> 00:01:41,360
sign and zero flag are the things that I

54
00:01:39,759 --> 00:01:43,439
really care about most of the time

55
00:01:41,360 --> 00:01:45,119
kind of reflected here in the fact that

56
00:01:43,439 --> 00:01:46,560
these assembly instructions dealing with

57
00:01:45,119 --> 00:01:49,280
sign and zero flags

58
00:01:46,560 --> 00:01:50,240
are some of the most common. Then we also

59
00:01:49,280 --> 00:01:52,479
have cmp

60
00:01:50,240 --> 00:01:54,159
coming in at 5% of the code

61
00:01:52,479 --> 00:01:56,320
because typically there's going to be

62
00:01:54,159 --> 00:01:59,600
cmp right before these conditional

63
00:01:56,320 --> 00:01:59,600
jump instructions.

