0
00:00:01,034 --> 00:00:04,023
So the takeaway from special maths dot C is that

1
00:00:04,023 --> 00:00:07,003
when the compiler happens to see some special math that

2
00:00:07,003 --> 00:00:09,099
it knows can be computed in that form that is

3
00:00:09,099 --> 00:00:14,008
like the base plus index time scale plus displacement that

4
00:00:14,008 --> 00:00:15,056
we learned about in the RMX form

5
00:00:15,084 --> 00:00:18,066
Then it can actually do that math using an L

6
00:00:18,066 --> 00:00:19,016
A

7
00:00:19,016 --> 00:00:22,026
Instead of doing the individual adds and multiplies

8
00:00:23,014 --> 00:00:26,031
Also a couple little miscellaneous things in case you missed

9
00:00:26,031 --> 00:00:26,056
it

10
00:00:27,004 --> 00:00:29,015
So there is a push our B X at the

11
00:00:29,015 --> 00:00:30,014
beginning of the function,

12
00:00:30,014 --> 00:00:31,092
and there is a pop R B X at the

13
00:00:31,092 --> 00:00:32,078
end of the function

14
00:00:32,079 --> 00:00:37,000
So what kind of saving of registers do we think

15
00:00:37,000 --> 00:00:37,059
that is?

16
00:00:37,006 --> 00:00:37,095
Well,

17
00:00:37,095 --> 00:00:39,076
I'll ask you in a quiz question in the second

18
00:00:40,024 --> 00:00:40,083
Also,

19
00:00:40,083 --> 00:00:43,089
you might notice that there is only a sub X

20
00:00:43,089 --> 00:00:45,089
20 instead of 28

21
00:00:45,095 --> 00:00:48,013
That probably has to do with the fact that there's

22
00:00:48,013 --> 00:00:49,062
this push our BX,

23
00:00:49,062 --> 00:00:51,028
which means that in total,

24
00:00:51,028 --> 00:00:54,097
the call the return address that's pushed onto the stack

25
00:00:54,098 --> 00:00:57,064
ends up getting balanced by this push our BX so

26
00:00:57,064 --> 00:01:00,059
that I think of Pilot sees it as 16 byte

27
00:01:00,059 --> 00:01:01,005
aligned

28
00:01:01,044 --> 00:01:03,056
So that's just another little interesting but there

29
00:01:04,004 --> 00:01:04,037
All right,

30
00:01:04,037 --> 00:01:06,088
So we've picked up yet another assembly instruction,

31
00:01:06,089 --> 00:01:07,076
Elia,

32
00:01:08,024 --> 00:01:10,087
and this one happens to be one of the larger

33
00:01:10,088 --> 00:01:13,039
5lices of this pie

34
00:01:13,004 --> 00:01:14,025
And at this point,

35
00:01:14,025 --> 00:01:16,048
we can go back to the Hello World,

36
00:01:16,049 --> 00:01:19,002
and we should actually be able to fully read this

37
00:01:19,002 --> 00:01:20,019
assembly at this point

38
00:01:20,002 --> 00:01:21,095
So what's going on in hello world?

39
00:01:22,034 --> 00:01:22,066
Well,

40
00:01:22,066 --> 00:01:25,022
there's the sub RSP 28 which we know is the

41
00:01:25,022 --> 00:01:29,038
combination of both stack alignment padding after the return address

42
00:01:29,038 --> 00:01:31,085
and the hex 20 for the shadow store area

43
00:01:32,024 --> 00:01:35,062
Then there is an ELA taking something and putting it

44
00:01:35,062 --> 00:01:36,046
into R C X

45
00:01:36,046 --> 00:01:36,073
Well,

46
00:01:36,073 --> 00:01:37,019
what is this?

47
00:01:37,019 --> 00:01:39,037
Something likely to be in the context of a print

48
00:01:39,037 --> 00:01:40,051
if it's probably the string

49
00:01:40,006 --> 00:01:43,006
But let's go ahead and confirm that now,

50
00:01:43,006 --> 00:01:44,086
using our disassembly and bugger

51
00:01:45,024 --> 00:01:48,097
So go ahead and set hello world as your startup

52
00:01:48,097 --> 00:01:51,005
project Start treaty Bugger,

53
00:01:51,074 --> 00:01:53,092
Go to the disassembly and let's go ahead and take

54
00:01:53,092 --> 00:01:55,058
a look at that memory location

55
00:01:55,058 --> 00:01:57,038
So I've turned off symbols here,

56
00:01:57,038 --> 00:01:59,045
so you don't see this listed as plaintiffs

57
00:01:59,094 --> 00:02:02,012
You don't see this listed with all that other weird

58
00:02:02,012 --> 00:02:02,055
stuff

59
00:02:03,004 --> 00:02:04,088
If you have the symbol names you'd see what we

60
00:02:04,088 --> 00:02:05,004
see before,

61
00:02:05,004 --> 00:02:07,004
but let's turn it off for a second so we

62
00:02:07,004 --> 00:02:09,046
can just see what the actual assembly instruction is

63
00:02:09,094 --> 00:02:11,063
So Sub RSP 28

64
00:02:11,063 --> 00:02:15,036
Let's go ahead and put our stack on eight byte

65
00:02:15,036 --> 00:02:19,016
size RSP automatically following along

66
00:02:19,074 --> 00:02:23,036
Then let's step into that instruction and RSP got moved

67
00:02:23,036 --> 00:02:24,076
by hex 28

68
00:02:24,081 --> 00:02:26,013
Then this next instruction

69
00:02:26,013 --> 00:02:27,005
What's going to happen with L

70
00:02:27,005 --> 00:02:27,018
E

71
00:02:27,018 --> 00:02:27,035
A

72
00:02:27,035 --> 00:02:27,065
Well,

73
00:02:27,065 --> 00:02:29,045
it's not going to use the square brackets to go

74
00:02:29,045 --> 00:02:29,083
to memory

75
00:02:29,083 --> 00:02:32,007
It's just going to literally take this value and move

76
00:02:32,007 --> 00:02:33,076
it into R C X

77
00:02:33,077 --> 00:02:36,011
But we came over here to the bugger to see

78
00:02:36,011 --> 00:02:37,002
what is that value

79
00:02:37,021 --> 00:02:40,000
So let's go ahead and step over this so that

80
00:02:40,000 --> 00:02:41,000
we can confirm that

81
00:02:41,000 --> 00:02:41,026
Yes,

82
00:02:41,026 --> 00:02:41,071
it does

83
00:02:41,071 --> 00:02:44,095
Put that exact value into RC X

84
00:02:45,094 --> 00:02:49,073
Then let's put our C X into our memory window

85
00:02:49,073 --> 00:02:50,032
And what is that?

86
00:02:50,032 --> 00:02:51,002
That memory address?

87
00:02:51,002 --> 00:02:51,005
Well,

88
00:02:51,005 --> 00:02:54,036
it looks like it's actually the SK String Hello world

89
00:02:54,094 --> 00:02:56,099
and why is it going into R C X?

90
00:02:57,000 --> 00:02:57,023
Well,

91
00:02:57,023 --> 00:02:58,096
we learn from our calling conventions

92
00:02:59,034 --> 00:03:02,065
That visual studio puts the first argument to a function

93
00:03:02,066 --> 00:03:03,065
into RC X,

94
00:03:03,067 --> 00:03:07,052
so RC X now holds a pointer to a string

95
00:03:07,053 --> 00:03:08,051
Hello World,

96
00:03:08,052 --> 00:03:11,093
which will be passed into the call to print

97
00:03:11,093 --> 00:03:13,063
If so,

98
00:03:13,063 --> 00:03:15,087
let's go ahead and step over that call to print

99
00:03:15,087 --> 00:03:20,004
If and we would see over here in our execute

100
00:03:20,004 --> 00:03:20,032
herbal,

101
00:03:20,032 --> 00:03:24,001
it prints out Hello world as expected and then moving

102
00:03:24,001 --> 00:03:29,029
12342 x Y e x because by convention TX or

103
00:03:29,029 --> 00:03:32,062
Rx is the place where you return the return value

104
00:03:32,062 --> 00:03:33,056
from a C function

105
00:03:34,004 --> 00:03:35,009
So step over that

106
00:03:35,001 --> 00:03:38,093
And there we go 1234 in our X and finally

107
00:03:38,093 --> 00:03:43,024
balancing out the subtraction of 28 from the stack

108
00:03:43,025 --> 00:03:45,066
We have to add back in 28 to the stack

109
00:03:46,014 --> 00:03:47,046
So do that

110
00:03:47,094 --> 00:03:49,002
And let's look at the stack

111
00:03:49,002 --> 00:03:51,088
And what we expect to see in a return instruction

112
00:03:51,089 --> 00:03:54,013
is the address of some code that we're going to

113
00:03:54,013 --> 00:03:54,093
return to

114
00:03:54,096 --> 00:03:57,075
This specifically is the code that called me

115
00:03:58,024 --> 00:03:58,009
So there we go

116
00:03:58,009 --> 00:03:59,086
Hello world,

117
00:03:59,087 --> 00:04:01,071
Just a few instructions

118
00:04:01,071 --> 00:04:03,093
But it takes us quite a while to understand exactly

119
00:04:03,093 --> 00:04:06,011
what all is going on here in terms of calling

120
00:04:06,011 --> 00:04:07,075
convention stack padding,

121
00:04:08,014 --> 00:04:09,076
Microsoft Shadow store,

122
00:04:10,054 --> 00:04:11,052
different argument,

123
00:04:11,052 --> 00:04:14,014
different registers that are used to pass back return values

124
00:04:14,014 --> 00:04:16,086
versus parameters and all of that good stuff

125
00:04:17,064 --> 00:04:19,013
So congratulations,

126
00:04:19,014 --> 00:04:20,006
You now understand

127
00:04:20,061 --> 00:04:21,056
Hello World

