1
00:00:00,080 --> 00:00:05,279
so let's take a look at specialmath.c

2
00:00:03,120 --> 00:00:07,600
it takes command line arguments so it's

3
00:00:05,279 --> 00:00:10,639
now starting to use the argument vector

4
00:00:07,600 --> 00:00:11,679
and it's using argv with atoi which

5
00:00:10,639 --> 00:00:14,000
expects in an

6
00:00:11,679 --> 00:00:15,839
integer as a string and converts it to

7
00:00:14,000 --> 00:00:17,920
an integer as an integer

8
00:00:15,839 --> 00:00:20,240
and in this particular case I've chosen

9
00:00:17,920 --> 00:00:22,480
some special math that I know will

10
00:00:20,240 --> 00:00:23,840
cause visual studio to generate the

11
00:00:22,480 --> 00:00:24,400
assembly instruction that I'm looking

12
00:00:23,840 --> 00:00:27,039
for

13
00:00:24,400 --> 00:00:28,880
which is lea now this is called special

14
00:00:27,039 --> 00:00:30,560
maths because I originally thought I was

15
00:00:28,880 --> 00:00:33,600
being funny because there was a

16
00:00:30,560 --> 00:00:36,160
politician who shall remain nameless who

17
00:00:33,600 --> 00:00:37,920
said maths instead of math which is

18
00:00:36,160 --> 00:00:39,760
funny to us americans because we say

19
00:00:37,920 --> 00:00:41,760
math we don't say maths

20
00:00:39,760 --> 00:00:43,440
now you can imagine my chagrin when I

21
00:00:41,760 --> 00:00:46,079
learned that the british and

22
00:00:43,440 --> 00:00:47,800
other folks who swear fealty to a queen

23
00:00:46,079 --> 00:00:50,719
happen to say maths

24
00:00:47,800 --> 00:00:52,879
unironically and so all of the irony is

25
00:00:50,719 --> 00:00:56,239
lost here but for the american audience

26
00:00:52,879 --> 00:00:58,320
special maths haha but enough about

27
00:00:56,239 --> 00:00:59,920
maths let's talk about lea

28
00:00:58,320 --> 00:01:01,440
so you can see here that it's using an

29
00:00:59,920 --> 00:01:03,920
r/mX form

30
00:01:01,440 --> 00:01:04,640
but back on the r/mX addressing slide I

31
00:01:03,920 --> 00:01:07,439
said

32
00:01:04,640 --> 00:01:08,080
Intel syntax, most of the time square

33
00:01:07,439 --> 00:01:09,600
brackets []

34
00:01:08,080 --> 00:01:11,840
means to treat the value within as a

35
00:01:09,600 --> 00:01:14,720
memory address and lea

36
00:01:11,840 --> 00:01:15,759
is the exception that proves the rule so

37
00:01:14,720 --> 00:01:19,200
specifically with

38
00:01:15,759 --> 00:01:20,560
lea load effective address you will see

39
00:01:19,200 --> 00:01:20,880
the square brackets form and you will

40
00:01:20,560 --> 00:01:24,000
see

41
00:01:20,880 --> 00:01:25,280
r/mX form but it is not actually going to

42
00:01:24,000 --> 00:01:28,320
memory at that address

43
00:01:25,280 --> 00:01:30,000
it is just calculating some value and

44
00:01:28,320 --> 00:01:32,320
treating it as if "oh i was just supposed

45
00:01:30,000 --> 00:01:34,560
to calculate a pointer value"

46
00:01:32,320 --> 00:01:35,360
and then it just loads that value into

47
00:01:34,560 --> 00:01:37,520
rax

48
00:01:35,360 --> 00:01:39,200
it doesn't dereference the memory

49
00:01:37,520 --> 00:01:41,040
it just treats it like it's a pointer

50
00:01:39,200 --> 00:01:43,280
address and loads the address

51
00:01:41,040 --> 00:01:45,600
loads the effective address as it were

52
00:01:43,280 --> 00:01:47,520
into a register for instance

53
00:01:45,600 --> 00:01:49,439
so frequently you will see this used

54
00:01:47,520 --> 00:01:51,119
with things like pointer arithmetic so

55
00:01:49,439 --> 00:01:52,079
if the C code is doing some point

56
00:01:51,119 --> 00:01:54,159
arithmetic

57
00:01:52,079 --> 00:01:56,000
that may turn into an lea because it can

58
00:01:54,159 --> 00:01:56,640
take you know some base register for a

59
00:01:56,000 --> 00:01:58,640
pointer

60
00:01:56,640 --> 00:02:00,560
plus whatever the pointer arithmetic was

61
00:01:58,640 --> 00:02:02,000
if you were trying to get the address of

62
00:02:00,560 --> 00:02:03,840
some element of an array

63
00:02:02,000 --> 00:02:05,040
well then it might generate an lea

64
00:02:03,840 --> 00:02:06,560
assembly instruction

65
00:02:05,040 --> 00:02:08,319
so I'm going to want you to go through

66
00:02:06,560 --> 00:02:08,800
this code but you can't go through this

67
00:02:08,319 --> 00:02:11,520
code

68
00:02:08,800 --> 00:02:13,200
unless you pass a command line parameter

69
00:02:11,520 --> 00:02:15,360
so in order to do that you

70
00:02:13,200 --> 00:02:17,520
right click on the project, go to its

71
00:02:15,360 --> 00:02:21,120
properties make sure you're in the debug

72
00:02:17,520 --> 00:02:23,840
Active(x64). Under the debugging entry

73
00:02:21,120 --> 00:02:25,920
you have command line command arguments

74
00:02:23,840 --> 00:02:27,200
and just put in something there like 16

75
00:02:25,920 --> 00:02:29,120
for instance

76
00:02:27,200 --> 00:02:31,120
all right so once again I must ask you

77
00:02:29,120 --> 00:02:32,959
for your support and helping me help

78
00:02:31,120 --> 00:02:35,280
you by having you go through the

79
00:02:32,959 --> 00:02:36,319
assembly now at this point making a

80
00:02:35,280 --> 00:02:38,080
stack diagram

81
00:02:36,319 --> 00:02:39,519
is not as important we've covered most

82
00:02:38,080 --> 00:02:41,920
of the stuff we're going to cover with

83
00:02:39,519 --> 00:02:43,519
respect to the stack and stack diagrams

84
00:02:41,920 --> 00:02:45,200
so I want to make sure that you

85
00:02:43,519 --> 00:02:47,760
understand the assembly understand how

86
00:02:45,200 --> 00:02:49,200
lea is being used so just step through

87
00:02:47,760 --> 00:02:50,000
the assembly and make sure you

88
00:02:49,200 --> 00:02:54,720
understand why

89
00:02:50,000 --> 00:02:54,720
all of those instructions are there

