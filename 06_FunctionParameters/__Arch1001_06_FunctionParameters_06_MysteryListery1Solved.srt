1
00:00:00,560 --> 00:00:04,240
so now we can go ahead and investigate

2
00:00:02,720 --> 00:00:06,640
our final mystery

3
00:00:04,240 --> 00:00:08,480
why do the GCC and Clang hello worlds

4
00:00:06,640 --> 00:00:10,559
have balanced push pop instructions but

5
00:00:08,480 --> 00:00:13,040
visual studio doesn't

6
00:00:10,559 --> 00:00:14,320
so we saw this assembly before and i've

7
00:00:13,040 --> 00:00:16,400
now moved it to my

8
00:00:14,320 --> 00:00:17,680
new and more beautiful color coding

9
00:00:16,400 --> 00:00:20,400
convention of

10
00:00:17,680 --> 00:00:21,920
blue for balanced and lavender for

11
00:00:20,400 --> 00:00:23,600
linkage

12
00:00:21,920 --> 00:00:25,199
so these are the assembly we saw before

13
00:00:23,600 --> 00:00:26,960
but now that we've seen the 32-bit

14
00:00:25,199 --> 00:00:28,960
calling conventions we might have a

15
00:00:26,960 --> 00:00:32,880
sense of why they might be doing this

16
00:00:28,960 --> 00:00:35,360
it's a push pop thing with a mov rbp

17
00:00:32,880 --> 00:00:37,840
sorry mov rsp to rbp. See? It gets you!

18
00:00:35,360 --> 00:00:38,800
the Intel syntax versus AT&T syntax it's

19
00:00:37,840 --> 00:00:42,160
backwards

20
00:00:38,800 --> 00:00:42,559
move rsp to rbp well that kind of sounds

21
00:00:42,160 --> 00:00:44,559
like

22
00:00:42,559 --> 00:00:45,840
it's the same thing that 32-bit calling

23
00:00:44,559 --> 00:00:48,399
conventions were doing

24
00:00:45,840 --> 00:00:51,520
so let's look into it so this is what we

25
00:00:48,399 --> 00:00:53,920
said about the Microsoft x64 ABI

26
00:00:51,520 --> 00:00:56,000
and what we now also say is that

27
00:00:53,920 --> 00:00:58,719
generally speaking it does not make use

28
00:00:56,000 --> 00:01:01,840
of frame pointers for most code

29
00:00:58,719 --> 00:01:04,159
so let's see what a 64-bit stack diagram

30
00:01:01,840 --> 00:01:08,479
would look like for visual studio

31
00:01:04,159 --> 00:01:10,400
return addresses as per usual rsp

32
00:01:08,479 --> 00:01:12,640
calling into a function return address

33
00:01:10,400 --> 00:01:14,479
gets onto the stack rsp points at local

34
00:01:12,640 --> 00:01:16,799
variables if any

35
00:01:14,479 --> 00:01:17,840
callee save registers, caller save

36
00:01:16,799 --> 00:01:20,080
registers

37
00:01:17,840 --> 00:01:21,119
and then the Microsoft specific shadow

38
00:01:20,080 --> 00:01:23,360
space

39
00:01:21,119 --> 00:01:24,960
and any extra function arguments above

40
00:01:23,360 --> 00:01:28,000
beyond the four parameters

41
00:01:24,960 --> 00:01:29,920
if any and then return addresses so this

42
00:01:28,000 --> 00:01:32,479
is kind of what we expect to see

43
00:01:29,920 --> 00:01:40,479
on stack frames for visual studio

44
00:01:32,479 --> 00:01:42,880
derived code

45
00:01:40,479 --> 00:01:43,600
and of course as before at a leaf node

46
00:01:42,880 --> 00:01:45,119
function

47
00:01:43,600 --> 00:01:47,119
we might have local variables, we might

48
00:01:45,119 --> 00:01:48,479
have callee save registers but we're not

49
00:01:47,119 --> 00:01:50,799
going to see any caller

50
00:01:48,479 --> 00:01:52,000
save registers or function parameters

51
00:01:50,799 --> 00:01:53,920
and so forth

52
00:01:52,000 --> 00:01:55,840
but now let's go look at the Microsoft

53
00:01:53,920 --> 00:01:57,600
documentation and see what they say

54
00:01:55,840 --> 00:01:59,439
about the stack frame layout

55
00:01:57,600 --> 00:02:00,880
and how it makes things a little bit

56
00:01:59,439 --> 00:02:03,040
more complicated so

57
00:02:00,880 --> 00:02:04,880
from this link here's a picture of the

58
00:02:03,040 --> 00:02:06,960
stack according to Microsoft

59
00:02:04,880 --> 00:02:08,160
they also draw high addresses high and

60
00:02:06,960 --> 00:02:11,520
low address is low so

61
00:02:08,160 --> 00:02:15,040
that's great for us here's the

62
00:02:11,520 --> 00:02:16,720
shadow store area of rcx rdx r8 r9 and

63
00:02:15,040 --> 00:02:17,200
then it says you know stack parameter

64
00:02:16,720 --> 00:02:19,520
area

65
00:02:17,200 --> 00:02:21,599
return addresses so this all maps up to

66
00:02:19,520 --> 00:02:24,160
what we expect local variables

67
00:02:21,599 --> 00:02:25,920
the non-volatile registers aka the

68
00:02:24,160 --> 00:02:27,280
callee saved registers

69
00:02:25,920 --> 00:02:29,680
but then there's this interesting bit

70
00:02:27,280 --> 00:02:31,519
right here it says if used frame pointer

71
00:02:29,680 --> 00:02:33,440
will generally point here

72
00:02:31,519 --> 00:02:35,680
so it's saying they can have a frame

73
00:02:33,440 --> 00:02:36,080
pointer but instead of pointing at the

74
00:02:35,680 --> 00:02:38,000
base

75
00:02:36,080 --> 00:02:39,599
of the frame it's actually going to be

76
00:02:38,000 --> 00:02:40,480
sort of in the middle here after the

77
00:02:39,599 --> 00:02:43,680
local variables

78
00:02:40,480 --> 00:02:45,680
after the callee save registers and

79
00:02:43,680 --> 00:02:47,440
before any allocation before any

80
00:02:45,680 --> 00:02:48,080
parameters and so forth so it's kind of

81
00:02:47,440 --> 00:02:50,800
a

82
00:02:48,080 --> 00:02:52,800
weird location so let's see what the

83
00:02:50,800 --> 00:02:55,920
actual filled in thing looks like

84
00:02:52,800 --> 00:02:57,360
so if they have a stack frame it's that

85
00:02:55,920 --> 00:03:00,720
it will generally point

86
00:02:57,360 --> 00:03:02,319
here the saved rbp will be between the

87
00:03:00,720 --> 00:03:05,680
local variables the callee save

88
00:03:02,319 --> 00:03:08,640
registers any alloca space

89
00:03:05,680 --> 00:03:09,680
and caller save registers so they can

90
00:03:08,640 --> 00:03:11,680
have this linkage

91
00:03:09,680 --> 00:03:13,120
but it'll be kind of into the middle so

92
00:03:11,680 --> 00:03:16,560
from the documentation

93
00:03:13,120 --> 00:03:18,480
it's clear that if you use alloca then

94
00:03:16,560 --> 00:03:20,400
it's definitely going to be using a

95
00:03:18,480 --> 00:03:22,000
frame pointer because it says alloca is

96
00:03:20,400 --> 00:03:23,840
required to be 16-byte aligned

97
00:03:22,000 --> 00:03:26,319
and additionally required to use a frame

98
00:03:23,840 --> 00:03:28,000
pointer. alloca is specifically a thing

99
00:03:26,319 --> 00:03:29,200
that allocates space on the stack

100
00:03:28,000 --> 00:03:31,440
instead of the heap

101
00:03:29,200 --> 00:03:33,200
and so if space is dynamically allocated

102
00:03:31,440 --> 00:03:35,120
for instance with alloca

103
00:03:33,200 --> 00:03:37,120
in a function then non-volatile

104
00:03:35,120 --> 00:03:38,000
registers must be used as a frame

105
00:03:37,120 --> 00:03:40,319
pointer that says

106
00:03:38,000 --> 00:03:42,000
a non-volatile register must be used as

107
00:03:40,319 --> 00:03:42,560
frame pointer it doesn't actually say it

108
00:03:42,000 --> 00:03:45,040
must be

109
00:03:42,560 --> 00:03:46,000
rbp it could actually be anyone and

110
00:03:45,040 --> 00:03:47,200
there's some other

111
00:03:46,000 --> 00:03:50,000
examples elsewhere in their

112
00:03:47,200 --> 00:03:50,959
documentation where they use r13 instead

113
00:03:50,000 --> 00:03:52,400
where they're kind of

114
00:03:50,959 --> 00:03:54,319
hand coding up you know here's an

115
00:03:52,400 --> 00:03:56,640
example of what you might make a

116
00:03:54,319 --> 00:03:57,599
function prologue look like. So anyways

117
00:03:56,640 --> 00:03:59,920
it's not currently

118
00:03:57,599 --> 00:04:01,760
clear to me from the documentation if

119
00:03:59,920 --> 00:04:03,599
Microsoft will naturally

120
00:04:01,760 --> 00:04:05,360
use the frame pointer in any other

121
00:04:03,599 --> 00:04:08,239
locations other than where

122
00:04:05,360 --> 00:04:09,519
some code happens to be using alloca

123
00:04:08,239 --> 00:04:12,640
but if you know about such

124
00:04:09,519 --> 00:04:14,720
situation then let me know! Now, here was 

125
00:04:12,640 --> 00:04:18,160
what we said about the System V

126
00:04:14,720 --> 00:04:20,799
x86-64 ABI and let's add to that

127
00:04:18,160 --> 00:04:22,960
that it still uses frame pointers

128
00:04:20,799 --> 00:04:25,199
basically in the 32-bit style

129
00:04:22,960 --> 00:04:26,320
unless directed not to via compiler

130
00:04:25,199 --> 00:04:28,800
options

131
00:04:26,320 --> 00:04:29,840
so therefore here's what the 64-bit

132
00:04:28,800 --> 00:04:32,479
stack would look like

133
00:04:29,840 --> 00:04:34,400
on System V. Call a function, return

134
00:04:32,479 --> 00:04:37,280
address as per usual

135
00:04:34,400 --> 00:04:39,680
and then saved rbp right at the base of

136
00:04:37,280 --> 00:04:42,320
the stack frame linking back up to

137
00:04:39,680 --> 00:04:42,800
wherever rbp used to point to and then

138
00:04:42,320 --> 00:04:45,040
the

139
00:04:42,800 --> 00:04:47,120
mov rsp to rbp and moving it down just

140
00:04:45,040 --> 00:04:49,680
like we saw in 32-bit

141
00:04:47,120 --> 00:04:51,360
so basically from there it looks the

142
00:04:49,680 --> 00:04:53,680
same as 32-bit

143
00:04:51,360 --> 00:04:54,560
all the same sort of things saved rbp,

144
00:04:53,680 --> 00:04:56,720
local variables,

145
00:04:54,560 --> 00:04:58,400
callee save, caller save, function arguments

146
00:04:56,720 --> 00:05:02,000
and return address

147
00:04:58,400 --> 00:05:03,440
and just keeps going as we saw before

148
00:05:02,000 --> 00:05:04,800
if you call another function you're

149
00:05:03,440 --> 00:05:05,919
going to have caller save registers

150
00:05:04,800 --> 00:05:08,639
potentially function

151
00:05:05,919 --> 00:05:10,080
arguments and return address and then

152
00:05:08,639 --> 00:05:13,120
for a leaf node

153
00:05:10,080 --> 00:05:15,039
saved rbp linking back as before moving

154
00:05:13,120 --> 00:05:17,600
rsp into rbp

155
00:05:15,039 --> 00:05:19,120
saving local variables callee save

156
00:05:17,600 --> 00:05:21,120
registers if appropriate

157
00:05:19,120 --> 00:05:23,600
but none of the rest of it if you're not

158
00:05:21,120 --> 00:05:26,000
actually calling any other functions

159
00:05:23,600 --> 00:05:28,880
so who was this presence of push pop in

160
00:05:26,000 --> 00:05:32,000
System V code absence in Microsoft code

161
00:05:28,880 --> 00:05:33,360
that's been terrorizing us so far?

162
00:05:32,000 --> 00:05:35,039
Well, it was none other than the

163
00:05:33,360 --> 00:05:38,720
difference between the Microsoft

164
00:05:35,039 --> 00:05:40,400
and System V ABIs. And with that

165
00:05:38,720 --> 00:05:41,759
our intrepid crew of the mystery machine

166
00:05:40,400 --> 00:05:44,000
has solved the last

167
00:05:41,759 --> 00:05:46,160
mystery in our mystery listery and it's

168
00:05:44,000 --> 00:05:51,360
time for all the cool cats and swingers

169
00:05:46,160 --> 00:05:51,360
to party out

