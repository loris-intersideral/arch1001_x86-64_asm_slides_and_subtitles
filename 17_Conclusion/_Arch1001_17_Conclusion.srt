1
00:00:00,719 --> 00:00:05,600
well well well it seems congratulations

2
00:00:03,199 --> 00:00:07,680
are in order you finished the class

3
00:00:05,600 --> 00:00:09,200
i want to thank you for sticking with it

4
00:00:07,680 --> 00:00:10,480
and joining me as you learn this

5
00:00:09,200 --> 00:00:12,240
interesting skill

6
00:00:10,480 --> 00:00:13,920
but i also want to remind you of what i

7
00:00:12,240 --> 00:00:15,120
said at the beginning of the class

8
00:00:13,920 --> 00:00:17,680
when you're done with the class you're

9
00:00:15,120 --> 00:00:19,840
not actually done there's so much more

10
00:00:17,680 --> 00:00:21,920
training and practice which is necessary

11
00:00:19,840 --> 00:00:23,519
to truly master this skill

12
00:00:21,920 --> 00:00:25,199
now hopefully you'll be able to get some

13
00:00:23,519 --> 00:00:27,039
of this practice via

14
00:00:25,199 --> 00:00:29,279
the subsequent classes which build on

15
00:00:27,039 --> 00:00:30,000
this material and it's ultimately my

16
00:00:29,279 --> 00:00:32,239
desire that

17
00:00:30,000 --> 00:00:34,160
you get a job in this area which lets

18
00:00:32,239 --> 00:00:35,440
you utilize these skills on a regular

19
00:00:34,160 --> 00:00:37,280
basis

20
00:00:35,440 --> 00:00:39,440
until then though i want to encourage

21
00:00:37,280 --> 00:00:41,360
you to keep leveling up keep

22
00:00:39,440 --> 00:00:42,719
learning those mad skills and climbing

23
00:00:41,360 --> 00:00:44,480
the skills tree

24
00:00:42,719 --> 00:00:46,239
because the more tools that you have in

25
00:00:44,480 --> 00:00:49,280
your toolbox the better engineer that

26
00:00:46,239 --> 00:00:49,280
you'll be

