1
00:00:00,080 --> 00:00:03,840
so whence cometh this reps toss well it

2
00:00:03,040 --> 00:00:06,960
turns out

3
00:00:03,840 --> 00:00:10,639
that that code is there because

4
00:00:06,960 --> 00:00:12,000
i purposely chose not to disable the

5
00:00:10,639 --> 00:00:14,160
runtime checks

6
00:00:12,000 --> 00:00:15,200
and so specifically it is this stack

7
00:00:14,160 --> 00:00:17,600
frame check

8
00:00:15,200 --> 00:00:18,480
which is being responsible for padding

9
00:00:17,600 --> 00:00:21,119
out the buffer

10
00:00:18,480 --> 00:00:22,960
on either side putting c's there and

11
00:00:21,119 --> 00:00:24,160
then checking at the end did somebody

12
00:00:22,960 --> 00:00:26,080
corrupt my c's

13
00:00:24,160 --> 00:00:28,320
because if they did then that means they

14
00:00:26,080 --> 00:00:30,320
have a stack buffer overflow

15
00:00:28,320 --> 00:00:32,079
now this is not actually a security

16
00:00:30,320 --> 00:00:32,880
check and you would not generally expect

17
00:00:32,079 --> 00:00:35,520
to see this

18
00:00:32,880 --> 00:00:37,440
in production code this is more just to

19
00:00:35,520 --> 00:00:39,040
help programmers know when they've

20
00:00:37,440 --> 00:00:39,920
screwed up and they're going to break

21
00:00:39,040 --> 00:00:42,239
something

22
00:00:39,920 --> 00:00:44,559
and it is this added complexity which is

23
00:00:42,239 --> 00:00:46,800
the reason why i specified that people

24
00:00:44,559 --> 00:00:48,559
should always be disabling all of these

25
00:00:46,800 --> 00:00:51,120
runtime checks in order to make our

26
00:00:48,559 --> 00:00:53,760
assembly code much simpler thus far

27
00:00:51,120 --> 00:00:54,399
if we do disable that runtime check then

28
00:00:53,760 --> 00:00:56,399
we will see

29
00:00:54,399 --> 00:00:58,320
much simpler code which we are more

30
00:00:56,399 --> 00:01:00,800
familiar with the typical

31
00:00:58,320 --> 00:01:02,000
overallocation of space for the shadow

32
00:01:00,800 --> 00:01:05,119
store

33
00:01:02,000 --> 00:01:09,680
indexing into our array at index hex

34
00:01:05,119 --> 00:01:13,360
27 which is 39 storing 42 to the array

35
00:01:09,680 --> 00:01:13,360
and returning hex blood

