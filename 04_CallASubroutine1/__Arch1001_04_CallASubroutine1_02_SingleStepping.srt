0
00:00:00,000 --> 00:00:05,839
so now for this first example only

1
00:00:03,360 --> 00:00:07,359
i want to give you sort of a slideware

2
00:00:05,839 --> 00:00:09,440
virtual debugging

3
00:00:07,359 --> 00:00:11,440
version of stepping through the assembly

4
00:00:09,440 --> 00:00:13,120
code and seeing what all changes

5
00:00:11,440 --> 00:00:14,240
so we're going to basically go through

6
00:00:13,120 --> 00:00:15,440
all of this code we're going to see what

7
00:00:14,240 --> 00:00:17,199
changes on the stack we're going to see

8
00:00:15,440 --> 00:00:19,680
what changes in the registers

9
00:00:17,199 --> 00:00:22,080
and so i have a particular key here that

10
00:00:19,680 --> 00:00:25,680
will apply to all these upcoming slides

11
00:00:22,080 --> 00:00:26,000
so this blue x or blue text will mean

12
00:00:25,680 --> 00:00:27,519
that

13
00:00:26,000 --> 00:00:29,119
that has been an assembly instruction

14
00:00:27,519 --> 00:00:30,880
which has just been executed

15
00:00:29,119 --> 00:00:34,480
we haven't actually executed any of them

16
00:00:30,880 --> 00:00:35,280
yet red values for modified values as a

17
00:00:34,480 --> 00:00:37,040
result of

18
00:00:35,280 --> 00:00:38,320
a particular assembly instruction

19
00:00:37,040 --> 00:00:41,760
execution and

20
00:00:38,320 --> 00:00:44,079
green for the starting values before

21
00:00:41,760 --> 00:00:44,879
anything has actually happened here so

22
00:00:44,079 --> 00:00:47,840
let's assume

23
00:00:44,879 --> 00:00:51,039
that the rip is set for one four zero

24
00:00:47,840 --> 00:00:53,120
zero ten ten aka the start of main

25
00:00:51,039 --> 00:00:54,079
and no assembly instruction has been

26
00:00:53,120 --> 00:00:56,160
executed yet

27
00:00:54,079 --> 00:00:57,520
so what are the initial values that we

28
00:00:56,160 --> 00:00:59,039
see

29
00:00:57,520 --> 00:01:00,640
and we'll say has one i mean these

30
00:00:59,039 --> 00:01:02,320
values are taken from the debugger later

31
00:01:00,640 --> 00:01:02,879
on so that they completely correspond to

32
00:01:02,320 --> 00:01:07,680
that

33
00:01:02,879 --> 00:01:09,840
but rax has one rsp is set to 14fe08 so

34
00:01:07,680 --> 00:01:12,080
somewhere up here on the stack

35
00:01:09,840 --> 00:01:13,520
and it has a particular value stored in

36
00:01:12,080 --> 00:01:14,880
it right now which is going to actually

37
00:01:13,520 --> 00:01:15,439
be the return address because we're

38
00:01:14,880 --> 00:01:16,960
saying

39
00:01:15,439 --> 00:01:18,960
we're on this line but we haven't

40
00:01:16,960 --> 00:01:21,680
actually executed it which means

41
00:01:18,960 --> 00:01:22,799
someone somewhere had to call main and

42
00:01:21,680 --> 00:01:25,119
when they call main

43
00:01:22,799 --> 00:01:26,000
a return address got placed onto the

44
00:01:25,119 --> 00:01:28,720
stack so that we

45
00:01:26,000 --> 00:01:30,000
can get back to whatever that code was

46
00:01:28,720 --> 00:01:34,400
that called made

47
00:01:30,000 --> 00:01:38,320
so rsp starts here all right now blue x

48
00:01:34,400 --> 00:01:40,079
we have executed sub rsp28

49
00:01:38,320 --> 00:01:41,680
so that's going to take rsp current

50
00:01:40,079 --> 00:01:44,240
value minus 28

51
00:01:41,680 --> 00:01:45,280
and store the value back into rsp so rsp

52
00:01:44,240 --> 00:01:48,479
was up here

53
00:01:45,280 --> 00:01:50,799
and now subtracts or goes down

54
00:01:48,479 --> 00:01:52,640
by hex 28 and we can see the

55
00:01:50,799 --> 00:01:55,119
corresponding change over here in the

56
00:01:52,640 --> 00:01:58,479
rsp register

57
00:01:55,119 --> 00:02:00,240
next assembly instruction call func

58
00:01:58,479 --> 00:02:02,640
and the particular address that funk is

59
00:02:00,240 --> 00:02:05,759
at is fourteen zero zero one

60
00:02:02,640 --> 00:02:06,240
one zero zero and so the side effect of

61
00:02:05,759 --> 00:02:08,399
the call

62
00:02:06,240 --> 00:02:09,360
assembly instruction we said is that a

63
00:02:08,399 --> 00:02:11,360
return address

64
00:02:09,360 --> 00:02:12,959
will get placed onto the stack so what

65
00:02:11,360 --> 00:02:16,239
is the return address one

66
00:02:12,959 --> 00:02:18,239
four thousand ten nineteen one

67
00:02:16,239 --> 00:02:20,080
four thousand ten nineteen is the

68
00:02:18,239 --> 00:02:22,720
address of the assembly instruction

69
00:02:20,080 --> 00:02:23,360
after the call assembly instruction so

70
00:02:22,720 --> 00:02:26,080
call

71
00:02:23,360 --> 00:02:27,360
we'll jump to function and when function

72
00:02:26,080 --> 00:02:29,040
returns back

73
00:02:27,360 --> 00:02:30,319
it's going to not go back to the call

74
00:02:29,040 --> 00:02:30,720
because that would cause an infinite

75
00:02:30,319 --> 00:02:32,879
loop

76
00:02:30,720 --> 00:02:35,360
it's going to go back to the assembly

77
00:02:32,879 --> 00:02:37,840
instruction after the call

78
00:02:35,360 --> 00:02:39,280
so rsp moved down it was up here and

79
00:02:37,840 --> 00:02:41,120
then it moved down by eight

80
00:02:39,280 --> 00:02:42,800
because of this return address being

81
00:02:41,120 --> 00:02:46,239
pushed onto the stack

82
00:02:42,800 --> 00:02:47,840
value change there as well all right

83
00:02:46,239 --> 00:02:49,440
next assembly instructions so you can

84
00:02:47,840 --> 00:02:52,560
see that the rip

85
00:02:49,440 --> 00:02:55,360
has moved up here to 14 00

86
00:02:52,560 --> 00:02:56,000
0 1000 now when this assembly

87
00:02:55,360 --> 00:02:57,760
instruction

88
00:02:56,000 --> 00:02:59,519
is executed it's not going to do

89
00:02:57,760 --> 00:03:00,720
anything to the stack no changes there

90
00:02:59,519 --> 00:03:03,440
no changes here

91
00:03:00,720 --> 00:03:04,159
but it is going to change the value of

92
00:03:03,440 --> 00:03:07,840
eax

93
00:03:04,159 --> 00:03:09,920
or rax to hex beef

94
00:03:07,840 --> 00:03:10,959
now you can't tell in this particular

95
00:03:09,920 --> 00:03:13,280
example but

96
00:03:10,959 --> 00:03:15,200
we you saw that the assembly moved it to

97
00:03:13,280 --> 00:03:17,280
eax not rax

98
00:03:15,200 --> 00:03:18,319
and so in reality when you move a value

99
00:03:17,280 --> 00:03:20,800
to eax

100
00:03:18,319 --> 00:03:22,159
it's going to actually zero extend the

101
00:03:20,800 --> 00:03:23,680
value so basically it's going to fill in

102
00:03:22,159 --> 00:03:26,640
the top 32 bits

103
00:03:23,680 --> 00:03:27,120
with zeros now previous versions of the

104
00:03:26,640 --> 00:03:28,959
class

105
00:03:27,120 --> 00:03:30,400
had you know just happened to have a

106
00:03:28,959 --> 00:03:31,840
nice rax value that

107
00:03:30,400 --> 00:03:34,080
showed that you were smashing the upper

108
00:03:31,840 --> 00:03:35,920
bits but this one it doesn't show so i

109
00:03:34,080 --> 00:03:37,680
just have to point it out to you

110
00:03:35,920 --> 00:03:40,720
and here's the section from the manual

111
00:03:37,680 --> 00:03:43,760
that says if you have a 32-bit operand

112
00:03:40,720 --> 00:03:45,120
uh it's going to zero extend into a

113
00:03:43,760 --> 00:03:48,080
64-bit result

114
00:03:45,120 --> 00:03:49,519
in the general purpose register now one

115
00:03:48,080 --> 00:03:52,080
quick point i want to make

116
00:03:49,519 --> 00:03:53,519
this zero extension only applies when

117
00:03:52,080 --> 00:03:56,000
you're writing to registers

118
00:03:53,519 --> 00:03:57,040
not to memory so if you move 32 bits

119
00:03:56,000 --> 00:03:58,959
into memory

120
00:03:57,040 --> 00:04:00,879
only 32 bits are going to change when

121
00:03:58,959 --> 00:04:04,000
you're moving this you know 32-bit

122
00:04:00,879 --> 00:04:05,760
operand the eax of

123
00:04:04,000 --> 00:04:07,439
a assembly instruction that's you know

124
00:04:05,760 --> 00:04:10,640
moving into a register

125
00:04:07,439 --> 00:04:12,080
then the code is then the hardware is

126
00:04:10,640 --> 00:04:13,920
going to behave as per the

127
00:04:12,080 --> 00:04:17,760
manual says and it's going to stick

128
00:04:13,920 --> 00:04:17,760
zeros into the upper 32 bits

129
00:04:18,079 --> 00:04:21,680
all right and then so just pausing here

130
00:04:20,079 --> 00:04:23,840
briefly we are

131
00:04:21,680 --> 00:04:25,360
having executed this assembly line we've

132
00:04:23,840 --> 00:04:28,479
put hex beef into

133
00:04:25,360 --> 00:04:29,759
eax rax and so what is the state of the

134
00:04:28,479 --> 00:04:31,360
stack right now

135
00:04:29,759 --> 00:04:33,840
well we said that the beginning of the

136
00:04:31,360 --> 00:04:35,600
stack was this return address back to

137
00:04:33,840 --> 00:04:38,479
whatever function

138
00:04:35,600 --> 00:04:39,759
was executing before main so this is

139
00:04:38,479 --> 00:04:41,280
part of the frame

140
00:04:39,759 --> 00:04:43,280
of the function that executed before

141
00:04:41,280 --> 00:04:43,919
main and then we have main which had you

142
00:04:43,280 --> 00:04:46,240
know some

143
00:04:43,919 --> 00:04:48,240
undefined big chunk of allocation that

144
00:04:46,240 --> 00:04:51,280
it did this rsp28

145
00:04:48,240 --> 00:04:52,160
so hex 28 worth of space right here and

146
00:04:51,280 --> 00:04:54,320
then when main

147
00:04:52,160 --> 00:04:55,840
called to funk it pushed a return

148
00:04:54,320 --> 00:04:57,280
address onto the stack so we consider

149
00:04:55,840 --> 00:05:00,639
those return addresses

150
00:04:57,280 --> 00:05:02,880
part of the calling functions frame

151
00:05:00,639 --> 00:05:04,560
so caller was right here was the

152
00:05:02,880 --> 00:05:05,919
function before main so the return

153
00:05:04,560 --> 00:05:08,240
address is part of its frame

154
00:05:05,919 --> 00:05:10,720
the caller here is main and so the

155
00:05:08,240 --> 00:05:13,680
return address is part of mains frame

156
00:05:10,720 --> 00:05:14,479
but we actually never see funk make use

157
00:05:13,680 --> 00:05:16,400
of

158
00:05:14,479 --> 00:05:18,720
its stack frame in any way because it's

159
00:05:16,400 --> 00:05:21,600
such a trivially simple function

160
00:05:18,720 --> 00:05:22,720
it just moves a value into eax or rax

161
00:05:21,600 --> 00:05:25,039
which is the

162
00:05:22,720 --> 00:05:26,160
return register by convention and then

163
00:05:25,039 --> 00:05:28,320
it's gonna return

164
00:05:26,160 --> 00:05:31,520
so basically there's never even a stack

165
00:05:28,320 --> 00:05:34,240
frame for function

166
00:05:31,520 --> 00:05:35,280
all right back to execution well return

167
00:05:34,240 --> 00:05:37,680
is now going to

168
00:05:35,280 --> 00:05:39,440
execute and so it's going to pop the

169
00:05:37,680 --> 00:05:41,039
value off the top of the stack what was

170
00:05:39,440 --> 00:05:43,280
that value it was this

171
00:05:41,039 --> 00:05:44,720
address of the assembly instruction

172
00:05:43,280 --> 00:05:47,039
after call

173
00:05:44,720 --> 00:05:48,800
so it's going to pop that value into rip

174
00:05:47,039 --> 00:05:50,160
so that the next code is going to

175
00:05:48,800 --> 00:05:51,600
execute here

176
00:05:50,160 --> 00:05:53,759
rather than you know for instance just

177
00:05:51,600 --> 00:05:55,600
falling through to here and then we're

178
00:05:53,759 --> 00:05:56,639
going to consider that undefined because

179
00:05:55,600 --> 00:06:00,319
the popping

180
00:05:56,639 --> 00:06:03,440
automatically increments rsp by 8

181
00:06:00,319 --> 00:06:05,759
and so now rsp is pointing right here

182
00:06:03,440 --> 00:06:07,120
next assembly instruction to execute is

183
00:06:05,759 --> 00:06:10,800
the one at 14

184
00:06:07,120 --> 00:06:12,800
000 1019 the move assembly instruction

185
00:06:10,800 --> 00:06:14,639
so you can see it doesn't even care what

186
00:06:12,800 --> 00:06:16,720
was returned in rax it's just going to

187
00:06:14,639 --> 00:06:17,440
immediately collaborate with hex food

188
00:06:16,720 --> 00:06:19,440
instead

189
00:06:17,440 --> 00:06:21,840
no changes to the stack on this

190
00:06:19,440 --> 00:06:25,199
particular assembly instruction

191
00:06:21,840 --> 00:06:28,080
and then add rsp plus 28 so we saw

192
00:06:25,199 --> 00:06:28,639
subtract here and then it's balancing it

193
00:06:28,080 --> 00:06:30,319
out

194
00:06:28,639 --> 00:06:31,840
so rather than using a bunch of push

195
00:06:30,319 --> 00:06:35,199
push push push push pop

196
00:06:31,840 --> 00:06:36,720
pop pop it's just subtracting the stack

197
00:06:35,199 --> 00:06:39,360
pointer down and adding the

198
00:06:36,720 --> 00:06:40,160
stack pointer back up to balance it out

199
00:06:39,360 --> 00:06:43,680
so that

200
00:06:40,160 --> 00:06:45,919
the next assembly instruction ret can

201
00:06:43,680 --> 00:06:46,800
pop the return address off the stack and

202
00:06:45,919 --> 00:06:50,240
go back to

203
00:06:46,800 --> 00:06:54,400
whichever function called main

204
00:06:50,240 --> 00:06:56,560
and that value was 14000 1349

205
00:06:54,400 --> 00:06:58,479
and again automatically as part of that

206
00:06:56,560 --> 00:07:02,479
return is the increment of

207
00:06:58,479 --> 00:07:04,319
rsp plus 8. so a couple of notes about

208
00:07:02,479 --> 00:07:06,960
that call of subroutine one

209
00:07:04,319 --> 00:07:08,240
the first is that obviously func is

210
00:07:06,960 --> 00:07:11,280
basically dead code

211
00:07:08,240 --> 00:07:13,199
it returns a value that is never used

212
00:07:11,280 --> 00:07:15,039
and practically speaking if you had any

213
00:07:13,199 --> 00:07:16,880
sort of optimizations on

214
00:07:15,039 --> 00:07:18,639
the compiler would know that this thing

215
00:07:16,880 --> 00:07:20,240
only ever returns hex food

216
00:07:18,639 --> 00:07:21,759
and it would just optimize it all out

217
00:07:20,240 --> 00:07:23,039
and throw it away but that's

218
00:07:21,759 --> 00:07:24,240
specifically why we

219
00:07:23,039 --> 00:07:26,000
turn off some of the compiler

220
00:07:24,240 --> 00:07:27,599
optimization so that we can show you

221
00:07:26,000 --> 00:07:31,039
simple assembly for simple c

222
00:07:27,599 --> 00:07:31,599
code also we don't really know why it

223
00:07:31,039 --> 00:07:35,039
did that

224
00:07:31,599 --> 00:07:36,720
sub rsp 28 and add rsp 28 didn't seem to

225
00:07:35,039 --> 00:07:39,280
have a lot of point to it

226
00:07:36,720 --> 00:07:41,039
you could imagine that for instance when

227
00:07:39,280 --> 00:07:41,680
it was right here it could have just not

228
00:07:41,039 --> 00:07:44,560
done that

229
00:07:41,680 --> 00:07:46,400
and then allowed main to you know push a

230
00:07:44,560 --> 00:07:47,039
return address here instead of like

231
00:07:46,400 --> 00:07:49,360
jumping down

232
00:07:47,039 --> 00:07:50,160
jumping back up so we're going to

233
00:07:49,360 --> 00:07:52,560
consider that

234
00:07:50,160 --> 00:07:53,680
yet another mystery for our mystery

235
00:07:52,560 --> 00:07:55,759
listery

236
00:07:53,680 --> 00:07:57,520
and we'll come back to that later on

237
00:07:55,759 --> 00:07:59,440
after we've learned a little bit more

238
00:07:57,520 --> 00:08:01,360
about how function calling works

239
00:07:59,440 --> 00:08:03,120
okay and building up our simple stack

240
00:08:01,360 --> 00:08:04,720
diagram a little bit more this is the

241
00:08:03,120 --> 00:08:07,680
same assembly code as we

242
00:08:04,720 --> 00:08:09,039
showed before and so what is the first

243
00:08:07,680 --> 00:08:10,720
thing that's going to go on to the stack

244
00:08:09,039 --> 00:08:12,479
it's going to be the return address of

245
00:08:10,720 --> 00:08:14,639
the thing that invokes maine which

246
00:08:12,479 --> 00:08:17,919
happens to be called invoke main

247
00:08:14,639 --> 00:08:19,039
push that onto the stack then main is

248
00:08:17,919 --> 00:08:22,240
going to have its own

249
00:08:19,039 --> 00:08:25,599
sort of notional frame and when

250
00:08:22,240 --> 00:08:27,440
main calls to foo it's going to have a

251
00:08:25,599 --> 00:08:29,759
return address pushed onto the stack

252
00:08:27,440 --> 00:08:31,919
when foo calls to bar it's going to have

253
00:08:29,759 --> 00:08:34,560
a return address pushed onto the stack

254
00:08:31,919 --> 00:08:36,320
and bar will have notionally a frame

255
00:08:34,560 --> 00:08:38,399
which will have some stuff stored in it

256
00:08:36,320 --> 00:08:39,919
that we don't know about yet

257
00:08:38,399 --> 00:08:42,000
so again this is the slightly more

258
00:08:39,919 --> 00:08:45,040
complex version now that we know about

259
00:08:42,000 --> 00:08:47,839
calling functions in c equals call ret

260
00:08:45,040 --> 00:08:49,680
assembly instructions we know that the

261
00:08:47,839 --> 00:08:52,000
stack frames are going to have

262
00:08:49,680 --> 00:08:54,640
return addresses inside the stack frame

263
00:08:52,000 --> 00:08:55,279
of whichever function calls the next

264
00:08:54,640 --> 00:08:57,680
function

265
00:08:55,279 --> 00:08:59,920
okay so we relatively quickly just added

266
00:08:57,680 --> 00:09:03,040
five new assembly instructions

267
00:08:59,920 --> 00:09:06,160
and so boom boom

268
00:09:03,040 --> 00:09:08,240
boom oh move was a lot look at that look

269
00:09:06,160 --> 00:09:10,800
at all that well that's not bad like we

270
00:09:08,240 --> 00:09:12,880
already only a couple of modules in

271
00:09:10,800 --> 00:09:15,279
already know well over 50

272
00:09:12,880 --> 00:09:16,399
of the assembly instructions that exist

273
00:09:15,279 --> 00:09:18,399
in web browsers

274
00:09:16,399 --> 00:09:21,360
so we are just chugging along at an

275
00:09:18,399 --> 00:09:21,360
incredible pace

