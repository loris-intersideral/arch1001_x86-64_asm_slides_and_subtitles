1
00:00:00,240 --> 00:00:04,640
so in order to avoid having our

2
00:00:02,480 --> 00:00:06,480
multipliers turned into shifts

3
00:00:04,640 --> 00:00:07,919
let's go ahead and use some non-powers

4
00:00:06,480 --> 00:00:11,120
of two so

5
00:00:07,919 --> 00:00:14,240
we've got defected times detectable

6
00:00:11,120 --> 00:00:16,160
divided by too bad so sad that's going

7
00:00:14,240 --> 00:00:18,320
to lead to the generation of a

8
00:00:16,160 --> 00:00:20,080
div instruction and an imul

9
00:00:18,320 --> 00:00:22,240
because as we said before

10
00:00:20,080 --> 00:00:24,480
visual studio really likes signed

11
00:00:22,240 --> 00:00:26,800
multiplies over unsigned multipliers

12
00:00:24,480 --> 00:00:28,640
so the unsigned divide instruction has

13
00:00:26,800 --> 00:00:30,320
three basic forms which I'm

14
00:00:28,640 --> 00:00:32,160
showing you all of just to show sort of

15
00:00:30,320 --> 00:00:33,120
the progression as things have extended

16
00:00:32,160 --> 00:00:36,160
over time

17
00:00:33,120 --> 00:00:39,440
the first form was the 16-bit ax

18
00:00:36,160 --> 00:00:41,440
divided by an 8-bit r/m8

19
00:00:39,440 --> 00:00:42,480
the quotient went into al and the

20
00:00:41,440 --> 00:00:46,280
remainder went into ah

21
00:00:42,480 --> 00:00:50,079
then in 32-bit mode there was the

22
00:00:46,280 --> 00:00:50,800
64-bit value edx concatenated with eax

23
00:00:50,079 --> 00:00:54,000
so that's the

24
00:00:50,800 --> 00:00:58,640
upper 64 bits in edx, lower 64 bits

25
00:00:54,000 --> 00:00:59,680
in eax divided by an r/m32 the quotient

26
00:00:58,640 --> 00:01:02,239
went to eax

27
00:00:59,680 --> 00:01:03,280
and the remainder went into edx and in

28
00:01:02,239 --> 00:01:06,880
64 bit

29
00:01:03,280 --> 00:01:07,840
same thing upper 64 bits of 128 value in

30
00:01:06,880 --> 00:01:12,080
rdx

31
00:01:07,840 --> 00:01:14,640
lower 64 and rax divided by an r/m64.

32
00:01:12,080 --> 00:01:15,920
quotient goes into rax, remainder goes

33
00:01:14,640 --> 00:01:18,320
into rdx

34
00:01:15,920 --> 00:01:18,960
if for instance you're in 32 bit mode

35
00:01:18,320 --> 00:01:21,439
but the

36
00:01:18,960 --> 00:01:22,720
dividend is actually only 32 bits and

37
00:01:21,439 --> 00:01:25,439
there's nothing going to be in

38
00:01:22,720 --> 00:01:26,240
edx for instance then the compiler could

39
00:01:25,439 --> 00:01:29,280
just you know

40
00:01:26,240 --> 00:01:30,960
zero out the upper bits as we see in the

41
00:01:29,280 --> 00:01:33,920
assembly that's generated

42
00:01:30,960 --> 00:01:34,799
for this example you can see there's an

43
00:01:33,920 --> 00:01:37,040
xor

44
00:01:34,799 --> 00:01:39,759
that zeros out the register and as

45
00:01:37,040 --> 00:01:40,079
expected if the divisor is 0 a divide

46
00:01:39,759 --> 00:01:42,079
by

47
00:01:40,079 --> 00:01:43,680
zero exception is raised because we're

48
00:01:42,079 --> 00:01:46,560
not allowed to divide by zero

49
00:01:43,680 --> 00:01:47,040
so quick simple examples in that 16-bit

50
00:01:46,560 --> 00:01:49,759
version

51
00:01:47,040 --> 00:01:51,360
ax let's say ax had the value 0x8

52
00:01:49,759 --> 00:01:54,479
divided by an r/m8

53
00:01:51,360 --> 00:01:58,159
which happens to be cx then

54
00:01:54,479 --> 00:02:01,280
8 divided by 3 is 2, remainder 2.

55
00:01:58,159 --> 00:02:03,759
or the longer version 8 divided by 5 is

56
00:02:01,280 --> 00:02:05,759
1, remainder 3. now the disassembler

57
00:02:03,759 --> 00:02:06,320
might actually write it like this you

58
00:02:05,759 --> 00:02:10,000
know div rax, rcx

59
00:02:06,320 --> 00:02:11,360
but the actual form

60
00:02:10,000 --> 00:02:15,680
it's the only thing that's actually

61
00:02:11,360 --> 00:02:17,920
being specified is the r/m64 or the r/m32

62
00:02:15,680 --> 00:02:20,319
these divide instructions always hard

63
00:02:17,920 --> 00:02:24,160
code that they must have the value in

64
00:02:20,319 --> 00:02:26,239
rdx, rax, edx, eax, etc

65
00:02:24,160 --> 00:02:27,920
they only use that as the thing that's

66
00:02:26,239 --> 00:02:30,000
going to be divided

67
00:02:27,920 --> 00:02:31,840
you can't specify any other registers to

68
00:02:30,000 --> 00:02:33,599
place it into so necessarily the

69
00:02:31,840 --> 00:02:36,319
compiler might have to move things

70
00:02:33,599 --> 00:02:37,120
into rdx rax before doing the actual

71
00:02:36,319 --> 00:02:38,879
division

72
00:02:37,120 --> 00:02:41,440
and then just to get out ahead of it the

73
00:02:38,879 --> 00:02:43,599
idiv is the signed version of div so

74
00:02:41,440 --> 00:02:46,560
if you were to go change the variables

75
00:02:43,599 --> 00:02:48,560
in MulDivExample to signed, then you

76
00:02:46,560 --> 00:02:51,040
would get an idiv of instead of a div

77
00:02:48,560 --> 00:02:52,319
exact same forms exact same hard coding

78
00:02:51,040 --> 00:02:54,959
of you know it must be

79
00:02:52,319 --> 00:02:56,319
this register combination divided by an

80
00:02:54,959 --> 00:02:58,239
r/m64

81
00:02:56,319 --> 00:03:00,000
same place as quotient and remainder and

82
00:02:58,239 --> 00:03:02,159
same results in terms of whether or not

83
00:03:00,000 --> 00:03:02,480
the compiler is going to you know zero

84
00:03:02,159 --> 00:03:05,840
out

85
00:03:02,480 --> 00:03:09,040
the upper bits if they're unused so 0xFE

86
00:03:05,840 --> 00:03:12,239
now this is the signed divide so 0xFE

87
00:03:09,040 --> 00:03:15,760
is -2 divided by

88
00:03:12,239 --> 00:03:16,159
2 gives you -1

89
00:03:15,760 --> 00:03:21,040
remainder 0

90
00:03:16,159 --> 00:03:21,040
or in 64 bits if you had stella

91
00:03:21,120 --> 00:03:25,120
yes I'm very sad, I didn't see that there

92
00:03:23,280 --> 00:03:26,239
was stella in hex words

93
00:03:25,120 --> 00:03:28,319
and so I really wish I would have

94
00:03:26,239 --> 00:03:28,720
incorporated this into our balboa blood

95
00:03:28,319 --> 00:03:32,159
blood

96
00:03:28,720 --> 00:03:35,920
examples maybe next time. If you take

97
00:03:32,159 --> 00:03:38,879
stella divided by bold, you get 0x7F

98
00:03:35,920 --> 00:03:41,200
with a remainder of 0x82B7. 10 internet

99
00:03:38,879 --> 00:03:42,000
points to anyone who can find me some

100
00:03:41,200 --> 00:03:45,200
words which

101
00:03:42,000 --> 00:03:46,799
when divided by other words yield either

102
00:03:45,200 --> 00:03:48,879
a word in the

103
00:03:46,799 --> 00:03:50,799
quotient or a word in the remainder so

104
00:03:48,879 --> 00:03:51,120
now is the time for all bold students to

105
00:03:50,799 --> 00:03:52,959
go

106
00:03:51,120 --> 00:03:58,720
forth and step through the assembly and

107
00:03:52,959 --> 00:03:58,720
check your understanding

