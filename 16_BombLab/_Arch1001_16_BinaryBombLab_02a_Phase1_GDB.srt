1
00:00:00,000 --> 00:00:04,080
all right this is the guide to the first

2
00:00:02,240 --> 00:00:07,200
phase of the bom lab

3
00:00:04,080 --> 00:00:09,679
using gdb as your primary debugger so

4
00:00:07,200 --> 00:00:10,480
to get the bom lab inside of your

5
00:00:09,679 --> 00:00:13,840
architecture

6
00:00:10,480 --> 00:00:17,440
1001 code for the class it will be

7
00:00:13,840 --> 00:00:20,400
in a folder called binary bom lab

8
00:00:17,440 --> 00:00:22,160
so go ahead and copy that to your home

9
00:00:20,400 --> 00:00:24,160
directory

10
00:00:22,160 --> 00:00:26,960
then let's just go ahead and run the bom

11
00:00:24,160 --> 00:00:26,960
and see what happens

12
00:00:27,599 --> 00:00:32,000
okay welcome to my finished little bomb

13
00:00:29,679 --> 00:00:32,719
you have six phases with which to blow

14
00:00:32,000 --> 00:00:35,840
yourself up

15
00:00:32,719 --> 00:00:38,800
have a nice day all right it seems to be

16
00:00:35,840 --> 00:00:41,360
expecting some sort of input

17
00:00:38,800 --> 00:00:42,320
and i enter input and boom the bomb has

18
00:00:41,360 --> 00:00:45,200
blown up

19
00:00:42,320 --> 00:00:46,800
i said the goal of this lab is to avoid

20
00:00:45,200 --> 00:00:48,879
the bomb blowing up so

21
00:00:46,800 --> 00:00:51,440
of course we could brute force it and

22
00:00:48,879 --> 00:00:54,719
put in every string in the world

23
00:00:51,440 --> 00:00:56,640
but that's not a good approach to this

24
00:00:54,719 --> 00:00:57,760
when faced with this sort of thing what

25
00:00:56,640 --> 00:01:00,719
does one do

26
00:00:57,760 --> 00:01:02,559
well one looks at the assembly okay well

27
00:01:00,719 --> 00:01:04,239
that's easy right let's just go ahead

28
00:01:02,559 --> 00:01:07,680
and you know obs dump it

29
00:01:04,239 --> 00:01:10,400
and you know disassemble it bom lab

30
00:01:07,680 --> 00:01:12,799
great let's just you know go and read

31
00:01:10,400 --> 00:01:17,040
all the assembly right

32
00:01:12,799 --> 00:01:19,360
it's so easy well pure static analysis

33
00:01:17,040 --> 00:01:20,640
is not necessarily always the best

34
00:01:19,360 --> 00:01:22,880
approach to things

35
00:01:20,640 --> 00:01:24,880
certainly because most programs are

36
00:01:22,880 --> 00:01:25,439
wildly more complicated than this bomb

37
00:01:24,880 --> 00:01:27,600
lab

38
00:01:25,439 --> 00:01:29,680
and you can see that even the bomb lab

39
00:01:27,600 --> 00:01:31,360
in and of itself has a ton of assembly

40
00:01:29,680 --> 00:01:33,600
to read

41
00:01:31,360 --> 00:01:35,600
so that's why when possible you should

42
00:01:33,600 --> 00:01:37,119
do dynamic analysis

43
00:01:35,600 --> 00:01:38,799
now when you're dealing with malware you

44
00:01:37,119 --> 00:01:40,720
might not want to do dynamic analysis

45
00:01:38,799 --> 00:01:42,399
because you might inadvertently affect

46
00:01:40,720 --> 00:01:44,479
infect yourself

47
00:01:42,399 --> 00:01:46,240
but for anything that's not known to be

48
00:01:44,479 --> 00:01:48,159
malware it like if you're reverse

49
00:01:46,240 --> 00:01:49,840
engineering some legitimate software to

50
00:01:48,159 --> 00:01:50,880
find vulnerabilities or just understand

51
00:01:49,840 --> 00:01:53,200
how it works

52
00:01:50,880 --> 00:01:54,799
it's always best to use dynamic analysis

53
00:01:53,200 --> 00:01:56,240
whenever humanly possible

54
00:01:54,799 --> 00:01:57,759
and ultimately it'll be you know a

55
00:01:56,240 --> 00:01:59,040
combination of dynamic and static

56
00:01:57,759 --> 00:02:01,680
analysis

57
00:01:59,040 --> 00:02:03,280
so we're going to use gdb on this bomb i

58
00:02:01,680 --> 00:02:04,159
guess i should say you know one of the

59
00:02:03,280 --> 00:02:06,880
other things that

60
00:02:04,159 --> 00:02:08,080
very frequently you should do is simply

61
00:02:06,880 --> 00:02:09,679
running strings

62
00:02:08,080 --> 00:02:11,599
and i may not actually have strings

63
00:02:09,679 --> 00:02:14,080
installed right now let's check okay

64
00:02:11,599 --> 00:02:14,080
good i do

65
00:02:15,120 --> 00:02:18,160
strings is kind of your first order go

66
00:02:17,760 --> 00:02:20,000
to

67
00:02:18,160 --> 00:02:21,280
in terms of figuring out what a program

68
00:02:20,000 --> 00:02:23,280
does you can

69
00:02:21,280 --> 00:02:25,840
see the kind of stuff that's inside of

70
00:02:23,280 --> 00:02:27,760
it based on you know what it prints out

71
00:02:25,840 --> 00:02:29,440
so we saw before that it prints out

72
00:02:27,760 --> 00:02:30,879
welcome to my finished little bomb you

73
00:02:29,440 --> 00:02:31,599
have six phases with which to pull

74
00:02:30,879 --> 00:02:33,840
yourself up

75
00:02:31,599 --> 00:02:35,519
have a nice day but you know maybe

76
00:02:33,840 --> 00:02:36,720
there's other clues in here you know

77
00:02:35,519 --> 00:02:39,360
that's number two

78
00:02:36,720 --> 00:02:39,920
keep going halfway there good work on to

79
00:02:39,360 --> 00:02:42,640
the next

80
00:02:39,920 --> 00:02:42,959
so there seems to be some sorts of you

81
00:02:42,640 --> 00:02:44,560
know

82
00:02:42,959 --> 00:02:46,480
interesting strings telling you

83
00:02:44,560 --> 00:02:49,200
something about how the thing works

84
00:02:46,480 --> 00:02:51,200
you can see that it originally came from

85
00:02:49,200 --> 00:02:53,280
cmu and so forth

86
00:02:51,200 --> 00:02:55,120
so probably you want to go take a look

87
00:02:53,280 --> 00:02:56,560
at the strings to figure out if there's

88
00:02:55,120 --> 00:03:00,159
any clues about how this

89
00:02:56,560 --> 00:03:02,560
particular program executes but let's go

90
00:03:00,159 --> 00:03:05,680
ahead and start it up in gdb

91
00:03:02,560 --> 00:03:06,640
so i'm going to cat my personal gdp

92
00:03:05,680 --> 00:03:09,360
command file

93
00:03:06,640 --> 00:03:11,440
or config file for what i have right now

94
00:03:09,360 --> 00:03:13,040
so this is just pretty much the default

95
00:03:11,440 --> 00:03:14,959
at this point we've got the

96
00:03:13,040 --> 00:03:17,040
printing out 10 assembly instructions

97
00:03:14,959 --> 00:03:19,519
we've got a variety of registers

98
00:03:17,040 --> 00:03:20,720
got the stack display and then a start

99
00:03:19,519 --> 00:03:24,159
to break at the

100
00:03:20,720 --> 00:03:25,120
main entry point okay so let's go ahead

101
00:03:24,159 --> 00:03:28,319
and run it

102
00:03:25,120 --> 00:03:33,840
gdb bom

103
00:03:28,319 --> 00:03:33,840
quiet and dash x and our commands file

104
00:03:34,799 --> 00:03:38,959
if we do that then we see it starts up

105
00:03:37,280 --> 00:03:41,680
and it prints out

106
00:03:38,959 --> 00:03:42,480
the first 10 assembly instructions add

107
00:03:41,680 --> 00:03:45,680
main

108
00:03:42,480 --> 00:03:47,040
some registers and the stack so here we

109
00:03:45,680 --> 00:03:49,760
are we have a very

110
00:03:47,040 --> 00:03:51,280
narrow window of view into what's going

111
00:03:49,760 --> 00:03:53,439
on so we probably want to

112
00:03:51,280 --> 00:03:55,840
widen that window a little bit could do

113
00:03:53,439 --> 00:03:58,319
that with disassemble

114
00:03:55,840 --> 00:04:00,080
all right so now we would just kind of

115
00:03:58,319 --> 00:04:03,120
skim through what's going on well

116
00:04:00,080 --> 00:04:03,840
we can see some setup a compare against

117
00:04:03,120 --> 00:04:07,040
two

118
00:04:03,840 --> 00:04:09,280
don't know what that is f open well f

119
00:04:07,040 --> 00:04:10,640
open has to do with file openings so

120
00:04:09,280 --> 00:04:12,080
there's probably some sort of file

121
00:04:10,640 --> 00:04:15,599
access in here

122
00:04:12,080 --> 00:04:17,759
i'll mention of initialize bomb a put s

123
00:04:15,599 --> 00:04:19,040
which we saw before is kind of like a

124
00:04:17,759 --> 00:04:22,240
printf

125
00:04:19,040 --> 00:04:25,919
that put us put us read line

126
00:04:22,240 --> 00:04:29,040
phase one phase diffused put s read line

127
00:04:25,919 --> 00:04:31,840
phase two face diffused so they said

128
00:04:29,040 --> 00:04:34,000
that there's you know six phases to this

129
00:04:31,840 --> 00:04:35,520
so that gives us a sense of what's going

130
00:04:34,000 --> 00:04:38,240
on here we've got this

131
00:04:35,520 --> 00:04:39,680
sequence of a phase being called and

132
00:04:38,240 --> 00:04:43,520
then a phase diffused

133
00:04:39,680 --> 00:04:45,840
after it all right so we could then

134
00:04:43,520 --> 00:04:48,240
for instance look at the disassembly for

135
00:04:45,840 --> 00:04:50,960
the first phase

136
00:04:48,240 --> 00:04:52,240
in order to see what's going on in there

137
00:04:50,960 --> 00:04:55,040
and we see

138
00:04:52,240 --> 00:04:56,080
a call to strings not equal and then we

139
00:04:55,040 --> 00:04:58,880
see a call to

140
00:04:56,080 --> 00:05:00,479
explode bomb so we might reasonably

141
00:04:58,880 --> 00:05:02,960
infer that there's some sort of

142
00:05:00,479 --> 00:05:05,280
string check and then a call to explode

143
00:05:02,960 --> 00:05:07,199
bomb if you give it the wrong string

144
00:05:05,280 --> 00:05:08,560
so let's go ahead and you know just

145
00:05:07,199 --> 00:05:11,840
start stepping through this

146
00:05:08,560 --> 00:05:14,960
and examining registers memory whatever

147
00:05:11,840 --> 00:05:17,840
looks interesting so in gdb

148
00:05:14,960 --> 00:05:19,199
how do we step well we can step assembly

149
00:05:17,840 --> 00:05:22,840
instruction at a time

150
00:05:19,199 --> 00:05:25,840
s i or we can do step overs with

151
00:05:22,840 --> 00:05:26,960
ni so let's go ahead and you know step

152
00:05:25,840 --> 00:05:30,400
step step

153
00:05:26,960 --> 00:05:33,600
just keep on going okay initialize bom

154
00:05:30,400 --> 00:05:36,080
all right stepped over that to do okay

155
00:05:33,600 --> 00:05:37,759
put s that put us just executed and here

156
00:05:36,080 --> 00:05:39,840
is the output from it

157
00:05:37,759 --> 00:05:40,800
that's the first chunk of that string

158
00:05:39,840 --> 00:05:44,400
and so

159
00:05:40,800 --> 00:05:47,440
continuing on next instruction next

160
00:05:44,400 --> 00:05:48,479
okay there's that other put s the rest

161
00:05:47,440 --> 00:05:51,120
of the string

162
00:05:48,479 --> 00:05:53,680
now we're about to call a read line so

163
00:05:51,120 --> 00:05:55,759
let's go ahead and step over that

164
00:05:53,680 --> 00:05:57,680
and then now we see that the debugger

165
00:05:55,759 --> 00:06:01,680
does not break again it does not just

166
00:05:57,680 --> 00:06:01,680
immediately go back to the debugger

167
00:06:02,080 --> 00:06:05,680
so it seems like this is the point at

168
00:06:04,000 --> 00:06:07,280
which it's going to be wanting us to put

169
00:06:05,680 --> 00:06:09,600
in some sort of input so i'm going to

170
00:06:07,280 --> 00:06:12,319
put in some sort of input

171
00:06:09,600 --> 00:06:12,639
and then i hit enter and now okay it

172
00:06:12,319 --> 00:06:14,080
went

173
00:06:12,639 --> 00:06:15,680
to the next assembly instruction

174
00:06:14,080 --> 00:06:18,160
afterwards

175
00:06:15,680 --> 00:06:19,520
so i've now given a string into this

176
00:06:18,160 --> 00:06:21,759
program and it should be

177
00:06:19,520 --> 00:06:23,199
sitting in here somewhere you know i

178
00:06:21,759 --> 00:06:25,360
could go analyze

179
00:06:23,199 --> 00:06:26,400
what that you know read line does in

180
00:06:25,360 --> 00:06:28,639
order to understand

181
00:06:26,400 --> 00:06:30,080
exactly where it puts it but let's just

182
00:06:28,639 --> 00:06:32,400
keep chugging for now

183
00:06:30,080 --> 00:06:34,479
let's just you know walk up to the next

184
00:06:32,400 --> 00:06:36,800
phase one and

185
00:06:34,479 --> 00:06:38,639
then when we're about to call this we

186
00:06:36,800 --> 00:06:40,560
could examine you know the different

187
00:06:38,639 --> 00:06:42,479
registers per the calling convention

188
00:06:40,560 --> 00:06:44,880
which we know are going to be argument

189
00:06:42,479 --> 00:06:46,080
zero argument one argument two ex so

190
00:06:44,880 --> 00:06:48,800
forth

191
00:06:46,080 --> 00:06:49,280
so the calling convention for system

192
00:06:48,800 --> 00:06:51,840
five

193
00:06:49,280 --> 00:06:52,400
says that the first argument to a

194
00:06:51,840 --> 00:06:55,360
function

195
00:06:52,400 --> 00:06:57,440
is in rdi now this phase one could take

196
00:06:55,360 --> 00:06:59,440
zero arguments so this rdi could be a

197
00:06:57,440 --> 00:07:02,160
complete red herring it could have

198
00:06:59,440 --> 00:07:04,400
nothing interesting nothing of value but

199
00:07:02,160 --> 00:07:06,400
you know just on a speculation

200
00:07:04,400 --> 00:07:09,199
let's you know see what what memory

201
00:07:06,400 --> 00:07:12,319
looks like at this location

202
00:07:09,199 --> 00:07:16,639
so i could examine memory with the

203
00:07:12,319 --> 00:07:20,080
x command and then i might say i want 10

204
00:07:16,639 --> 00:07:21,440
bytes in hexadecimal format and then

205
00:07:20,080 --> 00:07:24,800
give the address

206
00:07:21,440 --> 00:07:28,400
there oops no star there

207
00:07:24,800 --> 00:07:31,120
and so it says okay well whatever this

208
00:07:28,400 --> 00:07:34,880
thing is it seems to correspond to

209
00:07:31,120 --> 00:07:38,400
a symbol named input strings and we got

210
00:07:34,880 --> 00:07:41,759
69 6e 70 75

211
00:07:38,400 --> 00:07:44,720
74 0 000

212
00:07:41,759 --> 00:07:45,120
all right well i don't know what that is

213
00:07:44,720 --> 00:07:48,879
yet

214
00:07:45,120 --> 00:07:48,879
so let's just keep going

215
00:07:49,199 --> 00:07:54,560
so let's step into this particular phase

216
00:07:52,319 --> 00:07:54,560
one

217
00:07:54,879 --> 00:07:58,080
and then let's just keep stepping let's

218
00:07:57,280 --> 00:08:01,520
step

219
00:07:58,080 --> 00:08:04,479
step step and now we have a call to

220
00:08:01,520 --> 00:08:06,720
strings not equal well you might

221
00:08:04,479 --> 00:08:07,919
reasonably expect that a function called

222
00:08:06,720 --> 00:08:11,039
strings not equal

223
00:08:07,919 --> 00:08:12,560
is going to take two pointers to strings

224
00:08:11,039 --> 00:08:14,000
and it probably is going to check

225
00:08:12,560 --> 00:08:16,560
whether those strings are equal by

226
00:08:14,000 --> 00:08:18,879
comparing each byte within them

227
00:08:16,560 --> 00:08:20,319
so if that's my inference that i think

228
00:08:18,879 --> 00:08:22,000
strings not equal is going to take two

229
00:08:20,319 --> 00:08:26,000
pointers to two strings

230
00:08:22,000 --> 00:08:29,039
then i would expect that the rdi and rsi

231
00:08:26,000 --> 00:08:32,240
should be pointers to strings so

232
00:08:29,039 --> 00:08:35,839
testing that hypothesis i can examine

233
00:08:32,240 --> 00:08:39,120
those as strings xs

234
00:08:35,839 --> 00:08:39,599
and then that particular address if i do

235
00:08:39,120 --> 00:08:42,800
that

236
00:08:39,599 --> 00:08:44,959
i can see that actually the input is the

237
00:08:42,800 --> 00:08:47,200
input that i typed in when i was

238
00:08:44,959 --> 00:08:49,360
prompted for input in the debugger

239
00:08:47,200 --> 00:08:52,000
okay so basically now i know you know

240
00:08:49,360 --> 00:08:54,240
that thing that i was just looking at in

241
00:08:52,000 --> 00:08:55,440
bytes it would have been much better to

242
00:08:54,240 --> 00:08:58,320
look at it also

243
00:08:55,440 --> 00:08:59,600
in for instance characters at which

244
00:08:58,320 --> 00:09:01,360
point i would have seen oh these look

245
00:08:59,600 --> 00:09:03,680
like ascii characters

246
00:09:01,360 --> 00:09:05,120
and i could also look at it in strings

247
00:09:03,680 --> 00:09:07,360
and then boom i get a nice human

248
00:09:05,120 --> 00:09:10,399
readable string

249
00:09:07,360 --> 00:09:12,880
okay well the thing is called

250
00:09:10,399 --> 00:09:14,000
strings not equal so consequently i

251
00:09:12,880 --> 00:09:17,120
might expect that

252
00:09:14,000 --> 00:09:18,080
the other argument rsi is also going to

253
00:09:17,120 --> 00:09:20,399
be a string

254
00:09:18,080 --> 00:09:21,519
so let's go ahead and check what is at

255
00:09:20,399 --> 00:09:25,600
that string

256
00:09:21,519 --> 00:09:26,320
examine as string and the string i get

257
00:09:25,600 --> 00:09:29,360
there is

258
00:09:26,320 --> 00:09:29,760
i am just a renegade hockey mom all

259
00:09:29,360 --> 00:09:33,519
right

260
00:09:29,760 --> 00:09:35,600
that's interesting so we've got a thing

261
00:09:33,519 --> 00:09:38,240
called strings not equal that is taking

262
00:09:35,600 --> 00:09:40,720
my input and it's taking some other

263
00:09:38,240 --> 00:09:42,640
string i am just a renegade hockey mom

264
00:09:40,720 --> 00:09:43,920
and it's doing something with them who

265
00:09:42,640 --> 00:09:46,240
knows what it's doing with them it could

266
00:09:43,920 --> 00:09:49,040
be xoring them together it could be

267
00:09:46,240 --> 00:09:50,320
subtracting the sum of both of them from

268
00:09:49,040 --> 00:09:52,640
each other

269
00:09:50,320 --> 00:09:54,080
we could go and read the entire assembly

270
00:09:52,640 --> 00:09:56,399
for strings not equal

271
00:09:54,080 --> 00:09:57,279
or we can just step over it and see what

272
00:09:56,399 --> 00:09:59,040
happens next

273
00:09:57,279 --> 00:10:01,279
because if we look at the rest of phase

274
00:09:59,040 --> 00:10:03,839
one it's not too long right

275
00:10:01,279 --> 00:10:04,399
we've got phase one having a test of the

276
00:10:03,839 --> 00:10:06,320
output

277
00:10:04,399 --> 00:10:08,880
the you know return value of strings not

278
00:10:06,320 --> 00:10:11,839
equal and it's saying test eax

279
00:10:08,880 --> 00:10:14,240
eax so that's the return value and then

280
00:10:11,839 --> 00:10:15,920
jump if not equal

281
00:10:14,240 --> 00:10:17,600
so jump if not equal is the same thing

282
00:10:15,920 --> 00:10:20,560
as jump if not zero

283
00:10:17,600 --> 00:10:21,360
so when would the result of this be not

284
00:10:20,560 --> 00:10:23,920
zero

285
00:10:21,360 --> 00:10:25,040
well a test instruction is like an and

286
00:10:23,920 --> 00:10:27,120
instruction

287
00:10:25,040 --> 00:10:28,079
and so when do you get something that's

288
00:10:27,120 --> 00:10:31,120
not zero with an

289
00:10:28,079 --> 00:10:33,279
and instruction if the output is

290
00:10:31,120 --> 00:10:35,360
anything that's not zero if it's zero

291
00:10:33,279 --> 00:10:37,040
and zero it's going to be zero

292
00:10:35,360 --> 00:10:39,360
and then you would you know not take the

293
00:10:37,040 --> 00:10:40,240
jump not equal because it is equal it is

294
00:10:39,360 --> 00:10:42,160
zero

295
00:10:40,240 --> 00:10:44,320
but if it's anything other than zero

296
00:10:42,160 --> 00:10:48,079
that comes back out of this function

297
00:10:44,320 --> 00:10:48,079
then we're not going to take the jump

298
00:10:50,399 --> 00:10:53,920
and we're going to just fall through to

299
00:10:52,000 --> 00:10:56,880
this ad and then return

300
00:10:53,920 --> 00:10:59,040
so that's probably the way we want to go

301
00:10:56,880 --> 00:11:00,240
because this jump not equal if we look

302
00:10:59,040 --> 00:11:03,920
at the address

303
00:11:00,240 --> 00:11:05,200
5 5 c 4 that address is this address

304
00:11:03,920 --> 00:11:08,480
right here 5 5 c

305
00:11:05,200 --> 00:11:10,399
4 which is a call to explode bomb so

306
00:11:08,480 --> 00:11:12,320
that is definitely what we want to avoid

307
00:11:10,399 --> 00:11:13,680
we want to find a control flow path here

308
00:11:12,320 --> 00:11:16,720
that does not call

309
00:11:13,680 --> 00:11:19,120
explode bomb so in order to get past

310
00:11:16,720 --> 00:11:21,200
this we need to not take the jump in

311
00:11:19,120 --> 00:11:25,200
order to not take the jump we need

312
00:11:21,200 --> 00:11:25,200
this to return something that is not

313
00:11:26,839 --> 00:11:31,760
zero out of string is not equal

314
00:11:29,680 --> 00:11:33,360
and so let's see what it's currently

315
00:11:31,760 --> 00:11:35,600
returning

316
00:11:33,360 --> 00:11:36,800
let's go ahead and step over strings not

317
00:11:35,600 --> 00:11:39,600
equal

318
00:11:36,800 --> 00:11:42,240
and let's take a look at the rax

319
00:11:39,600 --> 00:11:45,839
register the eax register well

320
00:11:42,240 --> 00:11:47,279
that's one and so sorry i said it's we

321
00:11:45,839 --> 00:11:50,880
needed to jump if it's not

322
00:11:47,279 --> 00:11:51,839
equal we the jump is going to jump to

323
00:11:50,880 --> 00:11:53,839
the explode so

324
00:11:51,839 --> 00:11:55,519
we want to not take the jump so we want

325
00:11:53,839 --> 00:11:59,120
it to actually be equal

326
00:11:55,519 --> 00:12:01,360
zero so that it falls through so

327
00:11:59,120 --> 00:12:02,720
as we can see then if we just kind of

328
00:12:01,360 --> 00:12:06,800
step

329
00:12:02,720 --> 00:12:08,399
step boom we go to explode bomb

330
00:12:06,800 --> 00:12:10,240
all right well that is definitely not

331
00:12:08,399 --> 00:12:12,160
what we want so

332
00:12:10,240 --> 00:12:14,000
we don't exactly know yet what strings

333
00:12:12,160 --> 00:12:15,519
not equal does but we might you know

334
00:12:14,000 --> 00:12:17,200
reasonably infer that

335
00:12:15,519 --> 00:12:19,760
takes those two strings and compares if

336
00:12:17,200 --> 00:12:23,040
they're equal so over the two strings

337
00:12:19,760 --> 00:12:24,800
was input which i gave and then i am

338
00:12:23,040 --> 00:12:27,760
just a renegade hockey mom so

339
00:12:24,800 --> 00:12:30,000
i'm going to go ahead and try to provide

340
00:12:27,760 --> 00:12:32,320
i'm just a renegade hockey mom

341
00:12:30,000 --> 00:12:33,839
into this program and see what happens

342
00:12:32,320 --> 00:12:36,720
what gets returned from strings not

343
00:12:33,839 --> 00:12:40,079
equal if i provide that input

344
00:12:36,720 --> 00:12:42,000
so i can go ahead and rerun this program

345
00:12:40,079 --> 00:12:43,519
actually i should have done start rather

346
00:12:42,000 --> 00:12:45,200
than run so

347
00:12:43,519 --> 00:12:47,200
now it's just going to blow up i'm going

348
00:12:45,200 --> 00:12:49,680
to i want to keep control of it so let's

349
00:12:47,200 --> 00:12:52,560
go ahead and do start

350
00:12:49,680 --> 00:12:54,720
all right now i'm going to step over

351
00:12:52,560 --> 00:12:58,880
that read line call

352
00:12:54,720 --> 00:13:02,320
so where is that i could just you know

353
00:12:58,880 --> 00:13:04,240
run until after the read line so

354
00:13:02,320 --> 00:13:05,839
but i just have it in my copy buffer so

355
00:13:04,240 --> 00:13:07,360
i don't want to get rid of my copy

356
00:13:05,839 --> 00:13:11,519
buffer so i'm just going to

357
00:13:07,360 --> 00:13:13,680
step over it step step step step step

358
00:13:11,519 --> 00:13:15,440
step over the read line all right now

359
00:13:13,680 --> 00:13:17,279
it's asking for my input

360
00:13:15,440 --> 00:13:19,200
there you go i'm going to give i am just

361
00:13:17,279 --> 00:13:22,399
a renegade hockey mom

362
00:13:19,200 --> 00:13:25,440
hit enter and now we're going to go into

363
00:13:22,399 --> 00:13:29,600
phase one step into step into

364
00:13:25,440 --> 00:13:32,880
all right let's confirm that my input

365
00:13:29,600 --> 00:13:34,800
is in one of these input parameters rdi

366
00:13:32,880 --> 00:13:37,440
or rsi

367
00:13:34,800 --> 00:13:39,440
well last time is our rdi so i'll expect

368
00:13:37,440 --> 00:13:42,720
it's rdi this time as well

369
00:13:39,440 --> 00:13:44,480
examine as string first argument i'm

370
00:13:42,720 --> 00:13:47,600
just to renegade hockey mom

371
00:13:44,480 --> 00:13:48,560
great step over the string is not equal

372
00:13:47,600 --> 00:13:52,320
call

373
00:13:48,560 --> 00:13:52,800
and what is the output rax is equal to

374
00:13:52,320 --> 00:13:55,199
zero

375
00:13:52,800 --> 00:13:55,920
so it's going to do test which is and

376
00:13:55,199 --> 00:13:58,560
zero and

377
00:13:55,920 --> 00:14:00,560
zero it's going to jump if not zero but

378
00:13:58,560 --> 00:14:03,120
the result is definitely zero and zero

379
00:14:00,560 --> 00:14:03,839
is zero so it's not going to take the

380
00:14:03,120 --> 00:14:05,199
jump

381
00:14:03,839 --> 00:14:07,279
and it's going to fall through and

382
00:14:05,199 --> 00:14:09,680
return out of this phase

383
00:14:07,279 --> 00:14:11,440
so let's go ahead and step step there we

384
00:14:09,680 --> 00:14:14,480
go it did not call to

385
00:14:11,440 --> 00:14:17,519
explode bomb and step step

386
00:14:14,480 --> 00:14:18,639
and i'm back here phase diffused let me

387
00:14:17,519 --> 00:14:24,399
go ahead and step

388
00:14:18,639 --> 00:14:27,920
over that and just keep going

389
00:14:24,399 --> 00:14:30,000
all right some sort of printout

390
00:14:27,920 --> 00:14:33,120
phase one diffused how about the next

391
00:14:30,000 --> 00:14:35,040
one and then another read line

392
00:14:33,120 --> 00:14:36,720
next step over the read line and it

393
00:14:35,040 --> 00:14:40,240
wants input again so

394
00:14:36,720 --> 00:14:40,240
i don't know i'm gonna give input

395
00:14:40,560 --> 00:14:44,240
okay and this is where i'm actually

396
00:14:42,959 --> 00:14:45,519
going to leave you i'm not going to

397
00:14:44,240 --> 00:14:47,920
solve it all for you

398
00:14:45,519 --> 00:14:49,680
so at this point you need to figure out

399
00:14:47,920 --> 00:14:51,279
what input do you need to give to phase

400
00:14:49,680 --> 00:14:52,639
2 what input do you need to give to

401
00:14:51,279 --> 00:14:54,959
phase 3

402
00:14:52,639 --> 00:14:56,639
basically just read the assembly figure

403
00:14:54,959 --> 00:15:00,240
out what your exit conditions are

404
00:14:56,639 --> 00:15:01,920
avoid those calls to explode bomb and

405
00:15:00,240 --> 00:15:03,360
ultimately you want to complete to the

406
00:15:01,920 --> 00:15:06,000
entire program

407
00:15:03,360 --> 00:15:07,519
and successfully exit the bomb without

408
00:15:06,000 --> 00:15:09,440
it exploding

409
00:15:07,519 --> 00:15:10,959
but there was one extra little bit of

410
00:15:09,440 --> 00:15:13,279
information i forgot to tell you

411
00:15:10,959 --> 00:15:15,600
and that is that the bomb will actually

412
00:15:13,279 --> 00:15:15,920
take solutions in a text file that was

413
00:15:15,600 --> 00:15:17,760
the

414
00:15:15,920 --> 00:15:19,519
f open you see at the beginning it's

415
00:15:17,760 --> 00:15:21,760
basically opening a file and

416
00:15:19,519 --> 00:15:22,639
reading in lines from it so let's go

417
00:15:21,760 --> 00:15:26,399
ahead and make

418
00:15:22,639 --> 00:15:29,199
a dot txt let's put in our solution

419
00:15:26,399 --> 00:15:30,720
for the first phase i am just a renegade

420
00:15:29,199 --> 00:15:33,839
hockey mom

421
00:15:30,720 --> 00:15:37,440
and if we save that off and then we run

422
00:15:33,839 --> 00:15:39,360
the bom and a dot txt

423
00:15:37,440 --> 00:15:41,279
then you can see that it says phase one

424
00:15:39,360 --> 00:15:44,639
diffused how about the next one

425
00:15:41,279 --> 00:15:45,839
and you provide the input for the next

426
00:15:44,639 --> 00:15:48,560
phase

427
00:15:45,839 --> 00:15:49,199
so in order to basically run it each

428
00:15:48,560 --> 00:15:52,880
time

429
00:15:49,199 --> 00:15:56,720
with the a dot txt we can

430
00:15:52,880 --> 00:15:59,759
edit our gdb config file

431
00:15:56,720 --> 00:16:02,800
and change it to instead of start start

432
00:15:59,759 --> 00:16:04,560
start a dot txt and so this will mean

433
00:16:02,800 --> 00:16:07,279
every time you restart it

434
00:16:04,560 --> 00:16:08,320
it will have the appropriate solutions

435
00:16:07,279 --> 00:16:10,720
fed in

436
00:16:08,320 --> 00:16:12,959
also while we're here we probably want

437
00:16:10,720 --> 00:16:14,480
to add a break point for the next phase

438
00:16:12,959 --> 00:16:18,000
that we want to analyze

439
00:16:14,480 --> 00:16:20,720
so phase 2

440
00:16:18,000 --> 00:16:24,560
is where we want to stop next time so

441
00:16:20,720 --> 00:16:24,560
let's go ahead and see that in gdb

442
00:16:25,120 --> 00:16:30,399
run it like so okay well it breaks on

443
00:16:28,079 --> 00:16:33,759
main because of the start and we fed in

444
00:16:30,399 --> 00:16:37,440
the solutions after the start so we can

445
00:16:33,759 --> 00:16:41,440
go ahead and just continue

446
00:16:37,440 --> 00:16:44,160
and so it already diffused phase one

447
00:16:41,440 --> 00:16:45,920
so i can put in my phase two input and

448
00:16:44,160 --> 00:16:48,480
then now it's going to break on phase

449
00:16:45,920 --> 00:16:50,160
two because in my gdp config file i had

450
00:16:48,480 --> 00:16:53,759
a breakpoint set at phase

451
00:16:50,160 --> 00:16:54,160
two so there you go continue on from

452
00:16:53,759 --> 00:16:56,959
here

453
00:16:54,160 --> 00:16:56,959
on your own

