0
00:00:00,000 --> 00:00:04,240
now let's see some more examples of

1
00:00:02,159 --> 00:00:06,080
local variable usage which will actually

2
00:00:04,240 --> 00:00:07,520
cause new assembly instructions to

3
00:00:06,080 --> 00:00:09,920
appear so

4
00:00:07,520 --> 00:00:11,679
in this thing array local variable we've

5
00:00:09,920 --> 00:00:15,200
got three variables we've got

6
00:00:11,679 --> 00:00:17,199
short a we've got an integer array b

7
00:00:15,200 --> 00:00:18,640
which is six integers long and we've got

8
00:00:17,199 --> 00:00:20,080
a long long c

9
00:00:18,640 --> 00:00:22,000
and so we initialize some of these

10
00:00:20,080 --> 00:00:22,560
variables and you know as i'm talking to

11
00:00:22,000 --> 00:00:26,080
my wife in hex as I often do

12
00:00:22,560 --> 00:00:28,480
I say: "hey babe

13
00:00:26,080 --> 00:00:29,359
you know Balboa bled blood?"

14
00:00:28,480 --> 00:00:32,719
and she's like true true

15
00:00:29,359 --> 00:00:33,200
and so we go ahead and allocate

16
00:00:32,719 --> 00:00:36,559
these

17
00:00:33,200 --> 00:00:38,879
we take a and we stick it into b of one

18
00:00:36,559 --> 00:00:40,480
we take b of one and we add it to c put

19
00:00:38,879 --> 00:00:43,360
it into b of four and we return

20
00:00:40,480 --> 00:00:44,559
b of four as short this is the assembly

21
00:00:43,360 --> 00:00:46,239
that gets generated

22
00:00:44,559 --> 00:00:48,239
and we've got three new assembly

23
00:00:46,239 --> 00:00:49,920
instructions imul

24
00:00:48,239 --> 00:00:51,680
which is multiplication sign

25
00:00:49,920 --> 00:00:55,199
multiplication

26
00:00:51,680 --> 00:00:57,680
movsx which is

27
00:00:55,199 --> 00:00:58,800
signed extend which is move with sign

28
00:00:57,680 --> 00:01:01,359
extension

29
00:00:58,800 --> 00:01:03,280
and movzx which is move with zero

30
00:01:01,359 --> 00:01:05,280
extension

31
00:01:03,280 --> 00:01:06,720
all right first one: imul signed

32
00:01:05,280 --> 00:01:09,439
multiply

33
00:01:06,720 --> 00:01:11,680
so fyi visual studio really seems to

34
00:01:09,439 --> 00:01:14,960
like to use imul a lot more than

35
00:01:11,680 --> 00:01:16,320
other compilers so even some situations

36
00:01:14,960 --> 00:01:19,119
where you might expect to see

37
00:01:16,320 --> 00:01:20,000
you know unsigned multiply mul you're

38
00:01:19,119 --> 00:01:22,240
going to see imul

39
00:01:20,000 --> 00:01:24,159
that's kind of a quirk of visual studio

40
00:01:22,240 --> 00:01:25,600
which can be another way to identify

41
00:01:24,159 --> 00:01:26,080
when something is compiled in this

42
00:01:25,600 --> 00:01:28,640
particular

43
00:01:26,080 --> 00:01:29,119
compiler so there are three forms of 

44
00:01:28,640 --> 00:01:32,240
imul

45
00:01:29,119 --> 00:01:33,680
the first one will have a single operand

46
00:01:32,240 --> 00:01:35,600
and if you see that then what's

47
00:01:33,680 --> 00:01:36,720
happening is implicitly it's going to

48
00:01:35,600 --> 00:01:39,439
take that operand

49
00:01:36,720 --> 00:01:39,759
r/mX form and it's going to multiply it

50
00:01:39,439 --> 00:01:44,079
by rax

51
00:01:39,759 --> 00:01:48,000
and it's going to store the 128 bit

52
00:01:44,079 --> 00:01:51,600
value in rdx rax so the most significant

53
00:01:48,000 --> 00:01:52,720
64 bits in rdx least significant 64 bits

54
00:01:51,600 --> 00:01:54,159
in rax

55
00:01:52,720 --> 00:01:56,159
so that's one where it's just kind of

56
00:01:54,159 --> 00:01:58,079
implicit and you have to just know that

57
00:01:56,159 --> 00:02:01,680
an i'm all with single operand is going

58
00:01:58,079 --> 00:02:04,640
to be you know multiplying by rax

59
00:02:01,680 --> 00:02:05,759
second form with two operands register

60
00:02:04,640 --> 00:02:07,680
in an r/mX

61
00:02:05,759 --> 00:02:09,520
is going to do the more natural thing

62
00:02:07,680 --> 00:02:11,599
like we saw with add and subtract

63
00:02:09,520 --> 00:02:14,319
it's going to take register multiply it

64
00:02:11,599 --> 00:02:17,680
by r/mX and store it back into register

65
00:02:14,319 --> 00:02:19,760
and then there is a three operand form

66
00:02:17,680 --> 00:02:22,480
it has a register for destination it has

67
00:02:19,760 --> 00:02:25,040
an r/mX for a source and an immediate

68
00:02:22,480 --> 00:02:26,640
this will just take the r/mX and multiply

69
00:02:25,040 --> 00:02:27,760
it by the immediate and store it back

70
00:02:26,640 --> 00:02:29,120
into the register

71
00:02:27,760 --> 00:02:31,519
and this one's pretty interesting

72
00:02:29,120 --> 00:02:32,720
because three operands is not something

73
00:02:31,519 --> 00:02:35,519
that is common on

74
00:02:32,720 --> 00:02:36,160
intel assembly instructions as far as i

75
00:02:35,519 --> 00:02:38,640
know this

76
00:02:36,160 --> 00:02:40,080
is the only sort of basic assembly

77
00:02:38,640 --> 00:02:40,959
instruction meaning it's you know not

78
00:02:40,080 --> 00:02:43,360
one of these

79
00:02:40,959 --> 00:02:44,800
you know very large complicated extended

80
00:02:43,360 --> 00:02:47,440
assembly instruction sets

81
00:02:44,800 --> 00:02:48,480
that has a three operand form so here's

82
00:02:47,440 --> 00:02:51,680
some examples

83
00:02:48,480 --> 00:02:52,720
of using imul if we had again this one

84
00:02:51,680 --> 00:02:55,120
operand form

85
00:02:52,720 --> 00:02:56,640
implicitly it's going to imul rcx

86
00:02:55,120 --> 00:02:59,599
times rax

87
00:02:56,640 --> 00:03:00,239
so for instance we had rcx set to 4 and

88
00:02:59,599 --> 00:03:03,040
rax

89
00:03:00,239 --> 00:03:04,480
equal to some large 32-bit value and

90
00:03:03,040 --> 00:03:06,560
when we multiply that out

91
00:03:04,480 --> 00:03:09,440
the net result is going to be the upper

92
00:03:06,560 --> 00:03:10,080
100 the upper 64 bits of the 128 bit

93
00:03:09,440 --> 00:03:12,000
result

94
00:03:10,080 --> 00:03:13,519
are going to go in rdx and that actually

95
00:03:12,000 --> 00:03:16,959
does overflow the

96
00:03:13,519 --> 00:03:20,879
64-bit space and so 1 in rdx

97
00:03:16,959 --> 00:03:23,519
and one zero zero in rax as the result

98
00:03:20,879 --> 00:03:24,080
so in the two operand form it's the

99
00:03:23,519 --> 00:03:27,680
simple

100
00:03:24,080 --> 00:03:30,080
rax times rcx back into rax so

101
00:03:27,680 --> 00:03:31,040
let's just make it easy 4 times 20

102
00:03:30,080 --> 00:03:33,599
equals 80

103
00:03:31,040 --> 00:03:34,799
back into rax and then in the three

104
00:03:33,599 --> 00:03:37,840
operand form

105
00:03:34,799 --> 00:03:41,360
rcx times 6 back into rax

106
00:03:37,840 --> 00:03:42,959
so if rcx was 4, 4 times 6 is going to be

107
00:03:41,360 --> 00:03:46,319
24 which in hex

108
00:03:42,959 --> 00:03:48,720
is 18 back into rax

109
00:03:46,319 --> 00:03:50,319
the next instructions are move with zero

110
00:03:48,720 --> 00:03:52,799
extend and move with sign

111
00:03:50,319 --> 00:03:53,920
extent. These are frequently used to move

112
00:03:52,799 --> 00:03:57,360
values from

113
00:03:53,920 --> 00:03:58,959
small c data types into larger registers

114
00:03:57,360 --> 00:04:01,200
or memory locations

115
00:03:58,959 --> 00:04:02,640
dealing with the sign extension that may

116
00:04:01,200 --> 00:04:06,000
or may not be necessary

117
00:04:02,640 --> 00:04:08,480
so if you've got a signed short as we do

118
00:04:06,000 --> 00:04:10,159
in the example code and you want to

119
00:04:08,480 --> 00:04:11,599
you know move that or add that or

120
00:04:10,159 --> 00:04:12,879
subtract it or multiply it

121
00:04:11,599 --> 00:04:14,799
if you're going to do some operation

122
00:04:12,879 --> 00:04:16,239
with that signed short value

123
00:04:14,799 --> 00:04:18,880
and you're then going to do you know

124
00:04:16,239 --> 00:04:20,479
something with a signed 32-bit value

125
00:04:18,880 --> 00:04:22,240
you need to make sure that you sign

126
00:04:20,479 --> 00:04:25,120
extend that value

127
00:04:22,240 --> 00:04:26,720
before you do the operation so both of

128
00:04:25,120 --> 00:04:28,720
these forms

129
00:04:26,720 --> 00:04:30,880
both of these instructions support the

130
00:04:28,720 --> 00:04:33,280
same forms of register to register,

131
00:04:30,880 --> 00:04:34,800
register to memory, memory to register,

132
00:04:33,280 --> 00:04:35,600
immediate to memory and 

133
00:04:34,800 --> 00:04:37,120
immediate to register

134
00:04:35,600 --> 00:04:39,040
that we saw on the normal move

135
00:04:37,120 --> 00:04:41,360
instruction before

136
00:04:39,040 --> 00:04:42,400
and so the zero extension what that

137
00:04:41,360 --> 00:04:43,919
means is that

138
00:04:42,400 --> 00:04:45,440
the CPU will just automatically

139
00:04:43,919 --> 00:04:47,840
unconditionally take

140
00:04:45,440 --> 00:04:49,919
the smaller thing and stick zeros in

141
00:04:47,840 --> 00:04:52,320
front of it at the most significant bits

142
00:04:49,919 --> 00:04:53,280
in the destination whereas sign

143
00:04:52,320 --> 00:04:55,759
extension

144
00:04:53,280 --> 00:04:57,199
means that the CPU is going to fill in

145
00:04:55,759 --> 00:04:59,199
whatever the value is

146
00:04:57,199 --> 00:05:00,560
of the most significant bit of the thing

147
00:04:59,199 --> 00:05:02,800
that you're moving from

148
00:05:00,560 --> 00:05:05,199
so here's an example to illustrate that

149
00:05:02,800 --> 00:05:07,120
let's say that we had a 32-bit value

150
00:05:05,199 --> 00:05:09,680
food face and we moved it into the

151
00:05:07,120 --> 00:05:13,039
32-bit register eax

152
00:05:09,680 --> 00:05:16,160
if we did move with zero extension

153
00:05:13,039 --> 00:05:18,320
from eax to rbx

154
00:05:16,160 --> 00:05:20,080
then the zero extension would mean that

155
00:05:18,320 --> 00:05:22,080
the most significant bits would just

156
00:05:20,080 --> 00:05:23,520
automatically unconditionally always be

157
00:05:22,080 --> 00:05:25,840
set to zero

158
00:05:23,520 --> 00:05:27,440
on the other hand if we take you know

159
00:05:25,840 --> 00:05:30,160
move food face to eax

160
00:05:27,440 --> 00:05:31,039
and we move that with sign extension to

161
00:05:30,160 --> 00:05:33,759
rbx

162
00:05:31,039 --> 00:05:34,960
move with sign extension then the most

163
00:05:33,759 --> 00:05:38,000
significant bit

164
00:05:34,960 --> 00:05:39,840
the very you know 30 the index 31 the

165
00:05:38,000 --> 00:05:40,560
most significant bit of this 32-bit

166
00:05:39,840 --> 00:05:43,600
value

167
00:05:40,560 --> 00:05:45,120
is 1 in an f and that means it's going

168
00:05:43,600 --> 00:05:46,240
to fill in all ones in the most

169
00:05:45,120 --> 00:05:48,240
significant bits

170
00:05:46,240 --> 00:05:50,639
if it was instead a 0 it would fill in

171
00:05:48,240 --> 00:05:52,080
all zeros in the most significant bits

172
00:05:50,639 --> 00:05:53,840
it does this because of course when

173
00:05:52,080 --> 00:05:55,120
you're dealing with a value that is

174
00:05:53,840 --> 00:05:56,800
going to be signed

175
00:05:55,120 --> 00:05:58,560
if you want to make it you know a bigger

176
00:05:56,800 --> 00:05:59,199
holding for that it should still have

177
00:05:58,560 --> 00:06:01,280
you know all

178
00:05:59,199 --> 00:06:03,360
F's in order to keep it within the

179
00:06:01,280 --> 00:06:06,000
negative address range

180
00:06:03,360 --> 00:06:06,479
all right well those are how

181
00:06:06,000 --> 00:06:08,560
the imul

182
00:06:06,479 --> 00:06:09,520
and move assign extension and move with

183
00:06:08,560 --> 00:06:11,759
zero extension

184
00:06:09,520 --> 00:06:12,560
assembly instructions work. Now what I

185
00:06:11,759 --> 00:06:14,560
need you to do

186
00:06:12,560 --> 00:06:16,639
is go ahead and step through the

187
00:06:14,560 --> 00:06:18,800
assembly as before

188
00:06:16,639 --> 00:06:21,440
you know show where the local variables

189
00:06:18,800 --> 00:06:24,639
are and you know try to get a sense of

190
00:06:21,440 --> 00:06:24,639
what's initialized and what's

191
00:06:26,360 --> 00:06:29,360
uninitialized

