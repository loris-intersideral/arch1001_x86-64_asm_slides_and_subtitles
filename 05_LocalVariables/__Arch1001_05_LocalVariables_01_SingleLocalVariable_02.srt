1
00:00:00,000 --> 00:00:03,439
all right what you should see when you

2
00:00:01,920 --> 00:00:04,640
draw your stack diagram is something

3
00:00:03,439 --> 00:00:06,960
like this

4
00:00:04,640 --> 00:00:08,800
first there will be a return address on

5
00:00:06,960 --> 00:00:11,120
the stack immediately when you get into

6
00:00:08,800 --> 00:00:12,799
the first assembly instruction in main

7
00:00:11,120 --> 00:00:15,280
this will be your stack address and this

8
00:00:12,799 --> 00:00:17,680
will be the return address

9
00:00:15,280 --> 00:00:19,760
next main is going to allocate hex 28

10
00:00:17,680 --> 00:00:21,359
that's that mystery hex 28 that we don't

11
00:00:19,760 --> 00:00:22,800
really understand yet but we'll figure

12
00:00:21,359 --> 00:00:25,359
out later

13
00:00:22,800 --> 00:00:26,960
then main is going to call into func and

14
00:00:25,359 --> 00:00:30,320
that's going to cause it to push a

15
00:00:26,960 --> 00:00:32,480
return address onto the stack

16
00:00:30,320 --> 00:00:34,559
furthermore func is going to allocate

17
00:00:32,480 --> 00:00:37,680
hex 18 worth of space

18
00:00:34,559 --> 00:00:40,559
but then you'll see that i being a

19
00:00:37,680 --> 00:00:42,399
integer only has four bytes and so that

20
00:00:40,559 --> 00:00:45,760
hex scalable value goes

21
00:00:42,399 --> 00:00:47,600
onto the stack at fdc 0.

22
00:00:45,760 --> 00:00:49,360
but you might have you know drawn the

23
00:00:47,600 --> 00:00:51,280
actual literal value for this

24
00:00:49,360 --> 00:00:53,360
and this and this and this but the

25
00:00:51,280 --> 00:00:55,280
reality is if you don't see assembly

26
00:00:53,360 --> 00:00:57,120
initializing something which we don't

27
00:00:55,280 --> 00:00:59,440
for this address space or this address

28
00:00:57,120 --> 00:01:01,440
space that means it's actually undefined

29
00:00:59,440 --> 00:01:02,719
it's uninitialized space or it's padding

30
00:01:01,440 --> 00:01:04,960
space of some sort

31
00:01:02,719 --> 00:01:05,760
it's undefined and that's what we should

32
00:01:04,960 --> 00:01:08,240
actually draw there

33
00:01:05,760 --> 00:01:09,119
so in particular on scalable if you

34
00:01:08,240 --> 00:01:10,960
would have looked at

35
00:01:09,119 --> 00:01:13,280
the assembly instruction you would have

36
00:01:10,960 --> 00:01:16,320
seen that it says it's actually moving

37
00:01:13,280 --> 00:01:19,520
a dword pointer worth 32bit of

38
00:01:16,320 --> 00:01:21,759
data into that memory so i need to

39
00:01:19,520 --> 00:01:23,680
highlight an element of the rmx form

40
00:01:21,759 --> 00:01:26,159
that i skipped before

41
00:01:23,680 --> 00:01:27,759
specifically in intel syntax when you're

42
00:01:26,159 --> 00:01:30,479
using the rmx form

43
00:01:27,759 --> 00:01:32,079
it will actually annotate it with this

44
00:01:30,479 --> 00:01:34,159
portion at the beginning saying for

45
00:01:32,079 --> 00:01:35,280
instance keyword pointer dword pointer

46
00:01:34,159 --> 00:01:37,759
word pointer

47
00:01:35,280 --> 00:01:38,880
byte pointer etc and so that's

48
00:01:37,759 --> 00:01:41,119
essentially telling you

49
00:01:38,880 --> 00:01:42,960
in this move instruction that's moving

50
00:01:41,119 --> 00:01:45,040
some value to memory

51
00:01:42,960 --> 00:01:47,040
so here it's using immediates to memory

52
00:01:45,040 --> 00:01:48,640
here it's using a registered memory

53
00:01:47,040 --> 00:01:50,479
it's telling you what is the size of

54
00:01:48,640 --> 00:01:51,040
data which is going to be written to

55
00:01:50,479 --> 00:01:52,479
memory

56
00:01:51,040 --> 00:01:54,079
and i told you before when we were

57
00:01:52,479 --> 00:01:56,079
talking about uh

58
00:01:54,079 --> 00:01:58,399
you know values being written to

59
00:01:56,079 --> 00:02:00,399
registers i said that you know

60
00:01:58,399 --> 00:02:01,840
if you're moving a 32-bit value to a

61
00:02:00,399 --> 00:02:04,880
64-bit register

62
00:02:01,840 --> 00:02:06,960
intel you know expands that with zero

63
00:02:04,880 --> 00:02:08,080
extension but i said that doesn't apply

64
00:02:06,960 --> 00:02:10,959
to memory so

65
00:02:08,080 --> 00:02:12,640
here if you write exactly 32 bytes d

66
00:02:10,959 --> 00:02:15,520
word pointer worth of

67
00:02:12,640 --> 00:02:17,280
content to memory at rsp you're going to

68
00:02:15,520 --> 00:02:20,000
get exactly 32 bytes

69
00:02:17,280 --> 00:02:21,520
so let me show you a quick example of

70
00:02:20,000 --> 00:02:23,840
how you can

71
00:02:21,520 --> 00:02:25,520
for instance see the difference between

72
00:02:23,840 --> 00:02:27,840
things that are initialized and not

73
00:02:25,520 --> 00:02:30,239
initialized easily in visual studio

74
00:02:27,840 --> 00:02:32,879
so in single local variable if we go

75
00:02:30,239 --> 00:02:34,400
ahead and start the debugger

76
00:02:32,879 --> 00:02:36,400
i'm going to go ahead and continue and

77
00:02:34,400 --> 00:02:38,080
hit this breakpoint at func

78
00:02:36,400 --> 00:02:40,239
i'm going to go to the disassembly we

79
00:02:38,080 --> 00:02:42,160
can see this is that hex 18 worth of

80
00:02:40,239 --> 00:02:44,080
space being allocated on the stack

81
00:02:42,160 --> 00:02:46,080
i've got my memory window down here that

82
00:02:44,080 --> 00:02:48,000
is at rsp and it's continuously

83
00:02:46,080 --> 00:02:49,120
re-evaluating so that it always stay at

84
00:02:48,000 --> 00:02:52,000
rsp

85
00:02:49,120 --> 00:02:53,280
if i now step over this instruction it

86
00:02:52,000 --> 00:02:56,879
will have done the sub

87
00:02:53,280 --> 00:02:59,519
18 rsp i can actually click into

88
00:02:56,879 --> 00:03:01,200
the memory view here and i can just

89
00:02:59,519 --> 00:03:03,599
start typing ones

90
00:03:01,200 --> 00:03:04,800
and so i can go ahead and initialize

91
00:03:03,599 --> 00:03:08,080
this hex 18

92
00:03:04,800 --> 00:03:09,280
myself to all ones and now when i step

93
00:03:08,080 --> 00:03:11,599
through the assembly

94
00:03:09,280 --> 00:03:13,280
i will see when stuff changes so this

95
00:03:11,599 --> 00:03:15,120
next instruction is going to be moved

96
00:03:13,280 --> 00:03:17,040
scalable to rsp

97
00:03:15,120 --> 00:03:19,599
dword pointer size so it should be four

98
00:03:17,040 --> 00:03:21,920
bytes if i step over this instruction

99
00:03:19,599 --> 00:03:23,680
i will see scalable is there but this is

100
00:03:21,920 --> 00:03:25,760
all still ones meaning that you know it

101
00:03:23,680 --> 00:03:27,920
wasn't actually touched

102
00:03:25,760 --> 00:03:29,680
but i can further you know change this

103
00:03:27,920 --> 00:03:31,200
to a four byte view instead of an eight

104
00:03:29,680 --> 00:03:33,200
byte view and then it'll make it more

105
00:03:31,200 --> 00:03:34,239
clear that these ones were not touched

106
00:03:33,200 --> 00:03:36,959
whatsoever

107
00:03:34,239 --> 00:03:38,319
and these four bytes right here just had

108
00:03:36,959 --> 00:03:39,680
scalable move there

109
00:03:38,319 --> 00:03:41,519
so that's a little trick that'll

110
00:03:39,680 --> 00:03:42,560
possibly be helpful for you in

111
00:03:41,519 --> 00:03:44,879
determining

112
00:03:42,560 --> 00:03:46,159
uh what is you know initialized what is

113
00:03:44,879 --> 00:03:48,640
uninitialized

114
00:03:46,159 --> 00:03:50,239
but really the authoritative source is

115
00:03:48,640 --> 00:03:51,599
you know what are the sizes that are

116
00:03:50,239 --> 00:03:53,760
being specified

117
00:03:51,599 --> 00:03:56,720
uh in move instructions when things are

118
00:03:53,760 --> 00:03:58,239
being you know targeting an rmx form

119
00:03:56,720 --> 00:04:00,480
all right so what's our take away from

120
00:03:58,239 --> 00:04:00,799
single local variable well we can see

121
00:04:00,480 --> 00:04:02,720
that

122
00:04:00,799 --> 00:04:04,000
local variables are allocated on the

123
00:04:02,720 --> 00:04:06,480
stack so

124
00:04:04,000 --> 00:04:07,040
this assembly taking this value for what

125
00:04:06,480 --> 00:04:09,519
is clearly

126
00:04:07,040 --> 00:04:11,120
the local variable i putting it on to

127
00:04:09,519 --> 00:04:13,439
the stack at rsp

128
00:04:11,120 --> 00:04:14,560
means that you know local variables must

129
00:04:13,439 --> 00:04:16,720
exist on the stack

130
00:04:14,560 --> 00:04:18,000
but another big takeaway is that visual

131
00:04:16,720 --> 00:04:21,199
studio is clearly

132
00:04:18,000 --> 00:04:23,199
over allocating space on the stack for

133
00:04:21,199 --> 00:04:25,919
four bytes of data it's allocating hex

134
00:04:23,199 --> 00:04:27,759
18 and we don't know why that is

135
00:04:25,919 --> 00:04:29,280
so let's go ahead and add another thing

136
00:04:27,759 --> 00:04:31,440
to our mystery list

137
00:04:29,280 --> 00:04:34,560
why is visual studio over allocating

138
00:04:31,440 --> 00:04:36,320
space for a single local variable

139
00:04:34,560 --> 00:04:37,919
and one final thing we can say about

140
00:04:36,320 --> 00:04:39,280
local variables is now that we know that

141
00:04:37,919 --> 00:04:42,000
they're stored on the stack

142
00:04:39,280 --> 00:04:43,840
if we go back to this example foo car

143
00:04:42,000 --> 00:04:45,280
main calls foo calls bar

144
00:04:43,840 --> 00:04:47,600
where they've got each other single

145
00:04:45,280 --> 00:04:49,440
local variable with the stack growing

146
00:04:47,600 --> 00:04:50,639
downwards we have you know the return

147
00:04:49,440 --> 00:04:52,880
address to get from main

148
00:04:50,639 --> 00:04:53,840
back to the function that invoked main

149
00:04:52,880 --> 00:04:56,800
then main has

150
00:04:53,840 --> 00:04:58,720
its frame and that c that local variable

151
00:04:56,800 --> 00:05:01,440
is going to go somewhere on the stack

152
00:04:58,720 --> 00:05:01,759
for main's frame and when main calls to

153
00:05:01,440 --> 00:05:03,199
foo

154
00:05:01,759 --> 00:05:04,960
there's going to be a return address to

155
00:05:03,199 --> 00:05:08,240
get back to main and we have

156
00:05:04,960 --> 00:05:09,919
foo's frame, foo has a local variable b

157
00:05:08,240 --> 00:05:11,680
when foo calls bar it's going to have a

158
00:05:09,919 --> 00:05:15,280
return address on the stack

159
00:05:11,680 --> 00:05:17,440
and a bar has a single local variable

160
00:05:15,280 --> 00:05:19,039
that it uses on the stack but it doesn't

161
00:05:17,440 --> 00:05:22,240
call anything so it's not going to have

162
00:05:19,039 --> 00:05:22,240
any return address

